﻿Public Class jeuForm
    Dim pctTuile(30, 30) As PictureBox
    Dim tableauPctBox As PictureBox()
    Dim tableauPions As String() = {"CarreBleu", "CarreJaune", "CarreOrange", "CarreRouge", "CarreVert", "CarreViolet", "CroixBleue", "CroixJaune", "CroixOrange", "CroixRouge", "CroixVerte", "CroixViolet", "EtoileBleue", "EtoileJaune", "EtoileOrange", "EtoileRouge", "EtoileVerte", "EtoileViolet", "LosangeBleu", "LosangeJaune", "LosangeOrange", "LosangeRouge", "LosangeVert", "LosangeViolet", "RondBleu", "RondJaune", "RondOrange", "RondRouge", "RondVert", "RondViolet", "TrefleBleu", "TrefleJaune", "TrefleOrange", "TrefleRouge", "TrefleVert", "TrefleViolet"}
    Dim tableauCouleurs As String() = {"Bleu", "Bleue", "Jaune", "Orange", "Rouge", "Vert", "Verte", "Violet"}
    Dim tableauFormes As String() = {"Carre", "Croix", "Etoile", "Losange", "Rond", "Trefle"}
    Dim tableTuiles As New TableLayoutPanel()
    Public tableauNomJoueur(3) As Label
    Public tableauScoreJoueur(3) As Label
    Public ScoreJoueur(3) As String
    Dim numeroTuile As Integer
    Dim numberLastRow As Integer = 9
    Dim numberLastColumn As Integer = 9
    Dim variableBool As Boolean = False
    Dim pctBoxSender As PictureBox
    Dim temp As String
    Dim variable As String
    Dim pctTuilePose As String
    Dim lastPctTuilePoseForme As String
    Dim lastPctTuilePoseCouleur As String
    Dim tuile As String
    Dim oui As String
    Dim oui2 As String
    Dim non As String = 0
    Dim nbTuilesRestantes As Integer = 102


    Private Sub jeuForm_Load(sender As Object, e As EventArgs) Handles Me.Load
        tableauPctBox = {Me.pctBoxPioche0, Me.pctBoxPioche1, Me.pctBoxPioche2, Me.pctBoxPioche3, Me.pctBoxPioche4, Me.pctBoxPioche5}

        For i = 0 To 3
            tableauNomJoueur(i) = New Label
            tableauScoreJoueur(i) = New Label
            If i = 0 Then
                tableauNomJoueur(i).Location = New System.Drawing.Point(7, 25)
                tableauScoreJoueur(i).Location = New System.Drawing.Point(70, 25)
            Else
                tableauNomJoueur(i).Location = tableauNomJoueur(i - 1).Location + New System.Drawing.Point(0, 36)
                tableauScoreJoueur(i).Location = tableauScoreJoueur(i - 1).Location + New System.Drawing.Point(0, 36)
            End If

            tableauNomJoueur(i).Size = New Size(57, 13)
            tableauScoreJoueur(i).Size = New Size(20, 20)
            tableauNomJoueur(i).Text = MenuPrincipal.joueurNom(i)
            If String.IsNullOrEmpty(tableauNomJoueur(i).Text) Then
                tableauScoreJoueur(i).Text = Nothing
                ScoreJoueur(i) = Nothing
            Else
                tableauScoreJoueur(i).Text = 0
                ScoreJoueur(i) = 0
            End If
            grpboxJoueurs.Controls.Add(tableauNomJoueur(i))
            grpboxJoueurs.Controls.Add(tableauScoreJoueur(i))
        Next


        tableTuiles.Location = New System.Drawing.Point(185, 156)
        tableTuiles.Name = "tableTuiles"
        tableTuiles.Size = New System.Drawing.Size(500, 510)
        tableTuiles.ColumnCount = 30
        tableTuiles.RowCount = 30

        For i = 0 To 29 'Creation des 30 PicturesBox insérées dans la grille'

            For k = 0 To 29
                pctTuile(k, i) = New PictureBox
                pctTuile(k, i).Name = ("pctTuile" & k.ToString & i.ToString)
                pctTuile(k, i).BorderStyle = BorderStyle.FixedSingle
                pctTuile(k, i).Size = New Size(50, 51)
                pctTuile(k, i).AllowDrop = True
                tableTuiles.ColumnStyles.Add(New ColumnStyle(SizeType.AutoSize))
                tableTuiles.RowStyles.Add(New RowStyle(SizeType.AutoSize))
                AddHandler pctTuile(k, i).DragEnter, AddressOf pctTuile_DragEnter
                AddHandler pctTuile(k, i).DragDrop, AddressOf pctTuile_DragDrop
                AddHandler pctTuile(k, i).MouseMove, AddressOf pctTuile_MouseMove
                tableTuiles.Controls.Add(pctTuile(k, i), k, i)
                pctTuile(k, i).Margin = New Padding(0, 0, 0, 0)
            Next

        Next

        Controls.Add(tableTuiles)

        For i = 0 To 5 ' Pioche aléatoire du deck des 36 différents tuiles'
            Randomize()
            Dim random = CInt(Math.Ceiling(Rnd() * 35))
            tableauPctBox(i).Image = My.Resources.ResourceManager.GetObject(tableauPions(random))
            tableauPctBox(i).Name = tableauPions(random)
            tableauPctBox(i).AllowDrop = True
        Next
        pctTuile(5, 5).BackColor = Color.LightBlue
        MenuPrincipal.buttonScore.Enabled = True
    End Sub

    Private Sub pctBoxPioche0_MouseMove(sender As Object, e As MouseEventArgs) Handles pctBoxPioche5.MouseMove, pctBoxPioche4.MouseMove, pctBoxPioche3.MouseMove, pctBoxPioche2.MouseMove, pctBoxPioche1.MouseMove, pctBoxPioche0.MouseMove
        If e.Button = MouseButtons.Left Then
            sender.DoDragDrop(sender.Image, DragDropEffects.Move)
            sender.image = Nothing
            pctTuile(5, 5).BackColor = Nothing
            tuile = sender.name
        End If

    End Sub

    Private Sub pctTuile_DragEnter(sender As Object, e As DragEventArgs)
        If e.Data.GetDataPresent(DataFormats.Bitmap) Then
            e.Effect = DragDropEffects.Move
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub pctTuile_DragDrop(sender As Object, e As DragEventArgs)
        Dim image As Bitmap = DirectCast(e.Data.GetData(DataFormats.Bitmap, True), Bitmap)
        sender.Image = image
        For numeroTuile = 0 To 29
            ' Agrandissement de la grille et du form lorsque qu'une tuile est posée à l'extrémité haut '
            If sender.Name = ("pctTuile" & numeroTuile.ToString & "0") Then
                For i = 0 To 29
                    For j = 0 To 29
                        If pctTuile(j, i).Name = sender.Name Then
                            tableTuiles.Size = tableTuiles.Size + New System.Drawing.Size(0, 51)
                            Me.Size = Me.Size + New System.Drawing.Size(0, 51)
                            grpBoxTuiles.Location = grpBoxTuiles.Location + New System.Drawing.Point(0, 51)
                            numberLastRow = numberLastRow + 1
                            For k = 28 To 0 Step -1
                                For l = 29 To 0 Step -1
                                    If pctTuile(l, k).Image IsNot Nothing And pctTuile(l, k).Tag <> "1" Then
                                        pctTuile(l, k + 1).Image = pctTuile(l, k).Image
                                        pctTuile(l, k).Image = Nothing
                                        variableBool = True
                                    End If
                                    If variableBool = True Then
                                        pctTuile(l, k + 1).Tag = "1"
                                        variableBool = False
                                    End If
                                Next
                            Next

                            For k = 0 To 29
                                For l = 0 To 29
                                    pctTuile(l, k).Tag = "2"
                                Next
                            Next
                        End If
                    Next
                Next
                ' Agrandissement de la grille et du form lorsque qu'une tuile est posée à l'extrémité gauche '
            ElseIf sender.Name = ("pctTuile" & "0" & numeroTuile.ToString) Then
                For p = 0 To 29
                    For o = 0 To 29
                        If pctTuile(o, p).Name = sender.Name Then
                            tableTuiles.Size = tableTuiles.Size + New System.Drawing.Size(50, 0)
                            Me.Size = Me.Size + New System.Drawing.Size(50, 0)
                            grpBoxScore.Location = grpBoxScore.Location + New System.Drawing.Point(50, 0)
                            numberLastColumn = numberLastColumn + 1
                            For i = 29 To 0 Step -1
                                For j = 28 To 0 Step -1
                                    If pctTuile(j, i).Image IsNot Nothing And pctTuile(j, i).Tag <> "1" Then
                                        pctTuile(j + 1, i).Image = pctTuile(j, i).Image
                                        pctTuile(j, i).Image = Nothing
                                        variableBool = True
                                    End If
                                    If variableBool = True Then
                                        pctTuile(j + 1, i).Tag = "1"
                                        variableBool = False
                                    End If
                                Next
                            Next

                            For i = 0 To 29
                                For j = 0 To 29
                                    pctTuile(j, i).Tag = "2"
                                Next
                            Next
                        End If
                    Next
                Next
            End If
            ' Agrandissement de la grille et du form lorsque qu'une tuile est posée à l'extrémité bas '
            If sender.Name = ("pctTuile" & numeroTuile & numberLastRow) Then
                tableTuiles.Size = tableTuiles.Size + New System.Drawing.Size(0, 51)
                Me.Size = Me.Size + New System.Drawing.Size(0, 51)
                grpBoxTuiles.Location = grpBoxTuiles.Location + New System.Drawing.Point(0, 51)
                numberLastRow = numberLastRow + 1
            End If
            ' Agrandissement de la grille et du form lorsque qu'une tuile est posée à l'extrémité droite '
            If sender.Name = ("pctTuile" & numberLastColumn & numeroTuile) Then
                tableTuiles.Size = tableTuiles.Size + New System.Drawing.Size(50, 0)
                Me.Size = Me.Size + New System.Drawing.Size(50, 0)
                grpBoxScore.Location = grpBoxScore.Location + New System.Drawing.Point(50, 0)
                numberLastColumn = numberLastColumn + 1
            End If
        Next
    End Sub

    Private Sub pctTuile_MouseMove(sender As Object, e As MouseEventArgs)
        If e.Button = MouseButtons.Left Then
            sender.DoDragDrop(sender.Image, DragDropEffects.Move)
            sender.Image = Nothing
        End If
    End Sub

    Private Sub pctBoxPioche0_DragDrop(sender As Object, e As DragEventArgs) Handles pctBoxPioche5.DragDrop, pctBoxPioche4.DragDrop, pctBoxPioche3.DragDrop, pctBoxPioche2.DragDrop, pctBoxPioche1.DragDrop, pctBoxPioche0.DragDrop
        Dim image As Bitmap = DirectCast(e.Data.GetData(DataFormats.Bitmap, True), Bitmap)
        sender.Image = image
    End Sub

    Private Sub pctBoxPioche0_DragEnter(sender As Object, e As DragEventArgs) Handles pctBoxPioche5.DragEnter, pctBoxPioche4.DragEnter, pctBoxPioche3.DragEnter, pctBoxPioche2.DragEnter, pctBoxPioche1.DragEnter, pctBoxPioche0.DragEnter
        If e.Data.GetDataPresent(DataFormats.Bitmap) Then
            e.Effect = DragDropEffects.Move
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub btnMenu_Click(sender As Object, e As EventArgs) Handles btnMenu.Click
        MenuPrincipal.Show()
    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click
        For i = 0 To 5
            Randomize()
            Dim random = CInt(Math.Ceiling(Rnd() * 35))
            If tableauPctBox(i).Image Is Nothing Then
                tableauPctBox(i).Image = My.Resources.ResourceManager.GetObject(tableauPions(random))
                tableauPctBox(i).Name = tableauPions(random)
                nbTuilesRestantes = nbTuilesRestantes - 1
            End If
        Next
        labelNombreTuilesRest.Text = nbTuilesRestantes
    End Sub

    Private Sub btnRefuse_Click(sender As Object, e As EventArgs) Handles btnRefuse.Click

    End Sub

    Private Sub btnFindePartie_Click(sender As Object, e As EventArgs) Handles btnFindePartie.Click
        Me.Close()
    End Sub
End Class
