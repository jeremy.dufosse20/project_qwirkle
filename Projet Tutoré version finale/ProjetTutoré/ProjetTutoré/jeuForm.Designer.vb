﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class jeuForm
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(jeuForm))
        Me.btnMenu = New System.Windows.Forms.Button()
        Me.grpboxJoueurs = New System.Windows.Forms.GroupBox()
        Me.labelNombreTuilesRest = New System.Windows.Forms.Label()
        Me.labelTuilesRestantes = New System.Windows.Forms.Label()
        Me.btnFindePartie = New System.Windows.Forms.Button()
        Me.btnRefuse = New System.Windows.Forms.Button()
        Me.btnAccept = New System.Windows.Forms.Button()
        Me.pctBoxPioche0 = New System.Windows.Forms.PictureBox()
        Me.pctBoxPioche1 = New System.Windows.Forms.PictureBox()
        Me.pctBoxPioche2 = New System.Windows.Forms.PictureBox()
        Me.pctBoxPioche3 = New System.Windows.Forms.PictureBox()
        Me.pctBoxPioche4 = New System.Windows.Forms.PictureBox()
        Me.pctBoxPioche5 = New System.Windows.Forms.PictureBox()
        Me.grpBoxScore = New System.Windows.Forms.GroupBox()
        Me.grpBoxTuiles = New System.Windows.Forms.GroupBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.pctBoxPioche0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pctBoxPioche1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pctBoxPioche2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pctBoxPioche3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pctBoxPioche4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pctBoxPioche5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpBoxScore.SuspendLayout()
        Me.grpBoxTuiles.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnMenu
        '
        Me.btnMenu.Location = New System.Drawing.Point(28, 27)
        Me.btnMenu.Name = "btnMenu"
        Me.btnMenu.Size = New System.Drawing.Size(75, 23)
        Me.btnMenu.TabIndex = 0
        Me.btnMenu.Text = "< Menu"
        Me.btnMenu.UseVisualStyleBackColor = True
        '
        'grpboxJoueurs
        '
        Me.grpboxJoueurs.Location = New System.Drawing.Point(38, 28)
        Me.grpboxJoueurs.Name = "grpboxJoueurs"
        Me.grpboxJoueurs.Size = New System.Drawing.Size(120, 176)
        Me.grpboxJoueurs.TabIndex = 2
        Me.grpboxJoueurs.TabStop = False
        Me.grpboxJoueurs.Text = "Score"
        '
        'labelNombreTuilesRest
        '
        Me.labelNombreTuilesRest.AutoSize = True
        Me.labelNombreTuilesRest.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.labelNombreTuilesRest.Location = New System.Drawing.Point(16, 257)
        Me.labelNombreTuilesRest.Name = "labelNombreTuilesRest"
        Me.labelNombreTuilesRest.Size = New System.Drawing.Size(32, 17)
        Me.labelNombreTuilesRest.TabIndex = 3
        Me.labelNombreTuilesRest.Text = "102"
        '
        'labelTuilesRestantes
        '
        Me.labelTuilesRestantes.AutoSize = True
        Me.labelTuilesRestantes.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.labelTuilesRestantes.Location = New System.Drawing.Point(54, 257)
        Me.labelTuilesRestantes.Name = "labelTuilesRestantes"
        Me.labelTuilesRestantes.Size = New System.Drawing.Size(104, 17)
        Me.labelTuilesRestantes.TabIndex = 4
        Me.labelTuilesRestantes.Text = "tuiles restantes"
        '
        'btnFindePartie
        '
        Me.btnFindePartie.Location = New System.Drawing.Point(38, 308)
        Me.btnFindePartie.Name = "btnFindePartie"
        Me.btnFindePartie.Size = New System.Drawing.Size(120, 23)
        Me.btnFindePartie.TabIndex = 5
        Me.btnFindePartie.Text = "Fin de la partie"
        Me.btnFindePartie.UseVisualStyleBackColor = True
        '
        'btnRefuse
        '
        Me.btnRefuse.AllowDrop = True
        Me.btnRefuse.Image = CType(resources.GetObject("btnRefuse.Image"), System.Drawing.Image)
        Me.btnRefuse.Location = New System.Drawing.Point(604, 13)
        Me.btnRefuse.Name = "btnRefuse"
        Me.btnRefuse.Size = New System.Drawing.Size(90, 81)
        Me.btnRefuse.TabIndex = 16
        Me.btnRefuse.UseVisualStyleBackColor = True
        '
        'btnAccept
        '
        Me.btnAccept.Image = CType(resources.GetObject("btnAccept.Image"), System.Drawing.Image)
        Me.btnAccept.Location = New System.Drawing.Point(374, 13)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(90, 81)
        Me.btnAccept.TabIndex = 17
        Me.btnAccept.UseVisualStyleBackColor = True
        '
        'pctBoxPioche0
        '
        Me.pctBoxPioche0.BackColor = System.Drawing.SystemColors.Control
        Me.pctBoxPioche0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pctBoxPioche0.Location = New System.Drawing.Point(16, 30)
        Me.pctBoxPioche0.Name = "pctBoxPioche0"
        Me.pctBoxPioche0.Size = New System.Drawing.Size(51, 52)
        Me.pctBoxPioche0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pctBoxPioche0.TabIndex = 114
        Me.pctBoxPioche0.TabStop = False
        '
        'pctBoxPioche1
        '
        Me.pctBoxPioche1.BackColor = System.Drawing.SystemColors.Control
        Me.pctBoxPioche1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pctBoxPioche1.Location = New System.Drawing.Point(73, 30)
        Me.pctBoxPioche1.Name = "pctBoxPioche1"
        Me.pctBoxPioche1.Size = New System.Drawing.Size(51, 52)
        Me.pctBoxPioche1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pctBoxPioche1.TabIndex = 115
        Me.pctBoxPioche1.TabStop = False
        '
        'pctBoxPioche2
        '
        Me.pctBoxPioche2.BackColor = System.Drawing.SystemColors.Control
        Me.pctBoxPioche2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pctBoxPioche2.Location = New System.Drawing.Point(130, 30)
        Me.pctBoxPioche2.Name = "pctBoxPioche2"
        Me.pctBoxPioche2.Size = New System.Drawing.Size(51, 52)
        Me.pctBoxPioche2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pctBoxPioche2.TabIndex = 116
        Me.pctBoxPioche2.TabStop = False
        '
        'pctBoxPioche3
        '
        Me.pctBoxPioche3.BackColor = System.Drawing.SystemColors.Control
        Me.pctBoxPioche3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pctBoxPioche3.Location = New System.Drawing.Point(187, 30)
        Me.pctBoxPioche3.Name = "pctBoxPioche3"
        Me.pctBoxPioche3.Size = New System.Drawing.Size(51, 52)
        Me.pctBoxPioche3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pctBoxPioche3.TabIndex = 117
        Me.pctBoxPioche3.TabStop = False
        '
        'pctBoxPioche4
        '
        Me.pctBoxPioche4.BackColor = System.Drawing.SystemColors.Control
        Me.pctBoxPioche4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pctBoxPioche4.Location = New System.Drawing.Point(244, 30)
        Me.pctBoxPioche4.Name = "pctBoxPioche4"
        Me.pctBoxPioche4.Size = New System.Drawing.Size(51, 52)
        Me.pctBoxPioche4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pctBoxPioche4.TabIndex = 118
        Me.pctBoxPioche4.TabStop = False
        '
        'pctBoxPioche5
        '
        Me.pctBoxPioche5.BackColor = System.Drawing.SystemColors.Control
        Me.pctBoxPioche5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pctBoxPioche5.Location = New System.Drawing.Point(301, 30)
        Me.pctBoxPioche5.Name = "pctBoxPioche5"
        Me.pctBoxPioche5.Size = New System.Drawing.Size(51, 52)
        Me.pctBoxPioche5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pctBoxPioche5.TabIndex = 119
        Me.pctBoxPioche5.TabStop = False
        '
        'grpBoxScore
        '
        Me.grpBoxScore.BackColor = System.Drawing.Color.Transparent
        Me.grpBoxScore.Controls.Add(Me.grpboxJoueurs)
        Me.grpBoxScore.Controls.Add(Me.labelTuilesRestantes)
        Me.grpBoxScore.Controls.Add(Me.labelNombreTuilesRest)
        Me.grpBoxScore.Controls.Add(Me.btnFindePartie)
        Me.grpBoxScore.Location = New System.Drawing.Point(811, 79)
        Me.grpBoxScore.Name = "grpBoxScore"
        Me.grpBoxScore.Size = New System.Drawing.Size(203, 364)
        Me.grpBoxScore.TabIndex = 120
        Me.grpBoxScore.TabStop = False
        '
        'grpBoxTuiles
        '
        Me.grpBoxTuiles.BackColor = System.Drawing.Color.Transparent
        Me.grpBoxTuiles.Controls.Add(Me.PictureBox1)
        Me.grpBoxTuiles.Controls.Add(Me.pctBoxPioche0)
        Me.grpBoxTuiles.Controls.Add(Me.btnRefuse)
        Me.grpBoxTuiles.Controls.Add(Me.pctBoxPioche5)
        Me.grpBoxTuiles.Controls.Add(Me.btnAccept)
        Me.grpBoxTuiles.Controls.Add(Me.pctBoxPioche4)
        Me.grpBoxTuiles.Controls.Add(Me.pctBoxPioche1)
        Me.grpBoxTuiles.Controls.Add(Me.pctBoxPioche3)
        Me.grpBoxTuiles.Controls.Add(Me.pctBoxPioche2)
        Me.grpBoxTuiles.Location = New System.Drawing.Point(12, 770)
        Me.grpBoxTuiles.Name = "grpBoxTuiles"
        Me.grpBoxTuiles.Size = New System.Drawing.Size(718, 100)
        Me.grpBoxTuiles.TabIndex = 121
        Me.grpBoxTuiles.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(484, 7)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(99, 93)
        Me.PictureBox1.TabIndex = 120
        Me.PictureBox1.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(433, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(190, 55)
        Me.Label1.TabIndex = 122
        Me.Label1.Text = "Qwirkle"
        '
        'jeuForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(1026, 882)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.grpBoxTuiles)
        Me.Controls.Add(Me.grpBoxScore)
        Me.Controls.Add(Me.btnMenu)
        Me.Name = "jeuForm"
        Me.Text = "Form1"
        CType(Me.pctBoxPioche0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pctBoxPioche1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pctBoxPioche2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pctBoxPioche3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pctBoxPioche4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pctBoxPioche5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpBoxScore.ResumeLayout(False)
        Me.grpBoxScore.PerformLayout()
        Me.grpBoxTuiles.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnMenu As Button
    Friend WithEvents grpboxJoueurs As GroupBox
    Friend WithEvents labelNombreTuilesRest As Label
    Friend WithEvents labelTuilesRestantes As Label
    Friend WithEvents btnFindePartie As Button
    Friend WithEvents btnRefuse As Button
    Friend WithEvents btnAccept As Button
    Friend WithEvents pctBoxPioche0 As PictureBox
    Friend WithEvents pctBoxPioche1 As PictureBox
    Friend WithEvents pctBoxPioche2 As PictureBox
    Friend WithEvents pctBoxPioche3 As PictureBox
    Friend WithEvents pctBoxPioche4 As PictureBox
    Friend WithEvents pctBoxPioche5 As PictureBox
    Friend WithEvents grpBoxScore As GroupBox
    Friend WithEvents grpBoxTuiles As GroupBox
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents Label1 As Label
End Class
