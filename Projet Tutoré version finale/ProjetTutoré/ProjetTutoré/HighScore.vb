﻿Imports System.IO

Public Class Highscore
    Dim classement As New GroupBox()
    Dim Panel_General As New Panel()
    Dim i As Integer = 0
    Dim pxl As Integer = 0
    Dim tableau As New TableLayoutPanel()
    Dim j As Integer = 0
    Dim Place As Integer = 0
    Dim NomJoueur As String

    Dim Date_Score As Date = Date.Now()


    Private Sub label_score_Click(sender As Object, e As EventArgs) Handles label_score.Click

    End Sub



    Private Sub Highscore_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'Creation Tableau
        tableau.Location = New System.Drawing.Point(6, 19)
        tableau.Size = New System.Drawing.Size(523, 307)
        tableau.Name = "Tableau"
        tableau.ColumnCount = 1

        tableau.Visible = True


        'Creation GroupeBox Classement
        classement.Name = "Classement"
        'classement.BackColor = Color.ButtonFace
        'classement.ForeColor = Color.ControlText
        classement.Text = "Highscore"
        classement.Font = New Font("Microsoft Sans Serif", 8.25)
        classement.Location = New System.Drawing.Point(94, 3)
        classement.Size = New System.Drawing.Size(535, 352)
        'classement.Controls.Add(GB_Place)
        classement.Controls.Add(tableau)

        'Creation Panel General
        Panel_General.Name = "panel_general"
        Panel_General.Location = New System.Drawing.Point(12, 12)
        Panel_General.Size = New System.Drawing.Size(714, 409)
        Panel_General.AutoScroll = True
        Panel_General.BorderStyle = BorderStyle.FixedSingle

        Panel_General.VerticalScroll.Visible = True
        Panel_General.Controls.Add(classement)
        Me.Controls.Add(Panel_General)

    End Sub

    Private Sub GB_highscore_Enter(sender As Object, e As EventArgs) Handles GB_highscore.Enter

    End Sub


    Private Sub Envoyer_Score_Click(sender As Object, e As EventArgs) Handles Envoyer_Score.Click

        i = i + 1
        pxl = pxl + 70     'variable incrémentation + 70 pixel pour agrandir en fonction nombre de places

        Dim GB_Place2 As New GroupBox()
        Dim lb_count As New Label()
        Dim lb_NomJoueur2 As New Label()
        Dim lb_Score2 As New Label()
        Dim lb_Date2 As New Label()
        Dim Score As Integer = CInt(Int((200 * Rnd()) + 1)) 'valeur random entre 1 et 200


        If i >= 5 Then            'Si le nombre de scores est sup ou égale à 5, on agrandit la taille du tableau et du groupbox général
            tableau.Size = New System.Drawing.Size(523, 307 + pxl)
            classement.Size = New System.Drawing.Size(535, 352 + pxl)
        End If

        tableau.RowCount = i
        tableau.RowStyles.Add(New RowStyle(SizeType.AutoSize))


        'Creation Label NomJoueur
        lb_NomJoueur2.Name = ("nomjoueur" & j)
        lb_NomJoueur2.Text = MenuPrincipal.joueurNom(j)        'enlever guillemets 
        lb_NomJoueur2.Font = New Font("Microsoft Sans Serif", 9.75)
        lb_NomJoueur2.ForeColor = Color.DimGray
        lb_NomJoueur2.Location = New System.Drawing.Point(34, 31)
        lb_NomJoueur2.Size = New System.Drawing.Size(87, 16)

        'Creation Label Date
        lb_Date2.Name = "date"
        lb_Date2.Text = Date_Score  'enlever guillemets 
        lb_Date2.Font = New Font("Microsoft Sans Serif", 9.75)
        lb_Date2.ForeColor = Color.DimGray
        lb_Date2.Location = New System.Drawing.Point(399, 31)
        lb_Date2.Size = New System.Drawing.Size(60, 16)

        'Creation Label Score
        lb_Score2.Name = "score"
        lb_Score2.Text = jeuForm.ScoreJoueur(j)   'enlever guillemets 
        lb_Score2.Font = New Font("Microsoft Sans Serif", 9.75)
        lb_Score2.ForeColor = Color.DimGray
        lb_Score2.Location = New System.Drawing.Point(231, 31)
        lb_Score2.Size = New System.Drawing.Size(49, 16)

        'Creation GroupeBox GB_Place2
        GB_Place2.Name = ("gb_place2" & i)
        GB_Place2.Text = Place
        GB_Place2.Font = New Font("Microsoft Sans Serif", 12)
        GB_Place2.Location = New System.Drawing.Point(6, 95)
        GB_Place2.Size = New System.Drawing.Size(505, 70)
        GB_Place2.Controls.Add(lb_NomJoueur2)
        GB_Place2.Controls.Add(lb_Score2)
        GB_Place2.Controls.Add(lb_Date2)
        classement.Controls.Add(GB_Place2)

        tableau.Controls.Add(GB_Place2, 0, i)

        j = j + 1
    End Sub

End Class

