﻿Public Class MenuPrincipal
    Public joueurNom(3) As String
    Public joueurAge(3) As String
    Dim i As Integer = 0
    Private Sub Bouton_Règles_Click(sender As Object, e As EventArgs) Handles Bouton_Règles.Click
        Regle.Show()
    End Sub

    Private Sub MenuPrincipal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        buttonJouer.Enabled = False
        buttonScore.Enabled = False
    End Sub
    Private Sub Bouton_Selection2j_CheckedChanged(sender As Object, e As EventArgs) Handles Bouton_Selection2j.CheckedChanged

        TB_Nom_Joueur3.Enabled = False
        TB_Nom_Joueur3.Text = Nothing
        TB_Nom_Joueur4.Enabled = False
        TB_Nom_Joueur4.Text = Nothing

        TB_Age_Joueur3.Enabled = False
        TB_Age_Joueur3.Text = Nothing
        TB_Age_Joueur4.Enabled = False
        TB_Age_Joueur4.Text = Nothing

        If joueurNom(0) IsNot Nothing And joueurAge(0) <> Nothing Then
            If joueurNom(1) IsNot Nothing And joueurAge(1) <> Nothing Then
                buttonJouer.Enabled = True
            Else
                buttonJouer.Enabled = False
            End If
        Else
                buttonJouer.Enabled = False
            End If

    End Sub

    Private Sub Bouton_Selection3j_CheckedChanged(sender As Object, e As EventArgs) Handles Bouton_Selection3j.CheckedChanged

        TB_Nom_Joueur3.Enabled = True
        TB_Nom_Joueur4.Enabled = False
        TB_Nom_Joueur4.Text = Nothing

        TB_Age_Joueur3.Enabled = True
        TB_Age_Joueur4.Enabled = False
        TB_Age_Joueur4.Text = Nothing

        If joueurNom(0) IsNot Nothing And joueurAge(0) <> Nothing Then
            If joueurNom(1) IsNot Nothing And joueurAge(1) <> Nothing Then
                If joueurNom(2) IsNot Nothing And joueurAge(2) <> Nothing Then
                    buttonJouer.Enabled = True
                Else
                    buttonJouer.Enabled = False
                End If
            Else
                buttonJouer.Enabled = False
            End If
        End If
    End Sub

    Private Sub Bouton_Selection4j_CheckedChanged(sender As Object, e As EventArgs) Handles Bouton_Selection4j.CheckedChanged

        TB_Nom_Joueur3.Enabled = True
        TB_Nom_Joueur4.Enabled = True

        TB_Age_Joueur3.Enabled = True
        TB_Age_Joueur4.Enabled = True

        If joueurNom(0) IsNot Nothing And joueurAge(0) <> Nothing Then
            If joueurNom(1) IsNot Nothing And joueurAge(1) <> Nothing Then
                If joueurNom(2) IsNot Nothing And joueurAge(2) <> Nothing Then
                    If joueurNom(3) IsNot Nothing And joueurAge(3) <> Nothing Then
                        buttonJouer.Enabled = True
                    Else
                        buttonJouer.Enabled = False
                    End If
                End If
            Else
                buttonJouer.Enabled = False
            End If
        End If
    End Sub

    Private Sub Bouton_Quitter_Click(sender As Object, e As EventArgs) Handles Bouton_Quitter.Click

        Me.Close()

    End Sub

    Private Sub buttonJouer_Click(sender As Object, e As EventArgs) Handles buttonJouer.Click

        jeuForm.Show()

    End Sub

    Private Sub TB_Nom_Joueur1_TextChanged(sender As Object, e As EventArgs) Handles TB_Nom_Joueur1.TextChanged
        joueurNom(0) = sender.Text
    End Sub

    Private Sub TB_Nom_Joueur2_TextChanged(sender As Object, e As EventArgs) Handles TB_Nom_Joueur2.TextChanged
        joueurNom(1) = sender.Text
    End Sub

    Private Sub TB_Nom_Joueur3_TextChanged(sender As Object, e As EventArgs) Handles TB_Nom_Joueur3.TextChanged
        joueurNom(2) = sender.Text
    End Sub

    Private Sub TB_Nom_Joueur4_TextChanged(sender As Object, e As EventArgs) Handles TB_Nom_Joueur4.TextChanged
        joueurNom(3) = sender.Text
    End Sub

    Private Sub TB_Age_Joueur1_TextChanged(sender As Object, e As EventArgs) Handles TB_Age_Joueur1.TextChanged
        joueurAge(0) = sender.Text
    End Sub

    Private Sub TB_Age_Joueur2_TextChanged(sender As Object, e As EventArgs) Handles TB_Age_Joueur2.TextChanged
        joueurAge(1) = sender.Text
    End Sub

    Private Sub TB_Age_Joueur3_TextChanged(sender As Object, e As EventArgs) Handles TB_Age_Joueur3.TextChanged
        joueurAge(2) = sender.Text
    End Sub

    Private Sub TB_Age_Joueur4_TextChanged(sender As Object, e As EventArgs) Handles TB_Age_Joueur4.TextChanged
        joueurAge(3) = sender.Text
    End Sub

    Private Sub buttonScore_Click(sender As Object, e As EventArgs) Handles buttonScore.Click
        Highscore.Show()
    End Sub

End Class
