﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Menu
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.buttonJouer = New System.Windows.Forms.Button()
        Me.InfoJoueur = New System.Windows.Forms.GroupBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TB_Age_Joueur4 = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TB_Nom_Joueur4 = New System.Windows.Forms.TextBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TB_Age_Joueur3 = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TB_Nom_Joueur3 = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TB_Age_Joueur2 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TB_Nom_Joueur2 = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TB_Age_Joueur1 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TB_Nom_Joueur1 = New System.Windows.Forms.TextBox()
        Me.Bouton_Règles = New System.Windows.Forms.Button()
        Me.buttonRegles = New System.Windows.Forms.Button()
        Me.Bouton_Quitter = New System.Windows.Forms.Button()
        Me.Bouton_Selection2j = New System.Windows.Forms.RadioButton()
        Me.Bouton_Selection3j = New System.Windows.Forms.RadioButton()
        Me.Bouton_Selection4j = New System.Windows.Forms.RadioButton()
        Me.InfoJoueur.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(185, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(190, 55)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Qwirkle"
        '
        'buttonJouer
        '
        Me.buttonJouer.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.buttonJouer.Location = New System.Drawing.Point(238, 671)
        Me.buttonJouer.Name = "buttonJouer"
        Me.buttonJouer.Size = New System.Drawing.Size(130, 36)
        Me.buttonJouer.TabIndex = 1
        Me.buttonJouer.Text = "Jouer"
        Me.buttonJouer.UseVisualStyleBackColor = True
        '
        'InfoJoueur
        '
        Me.InfoJoueur.Controls.Add(Me.Panel4)
        Me.InfoJoueur.Controls.Add(Me.Panel3)
        Me.InfoJoueur.Controls.Add(Me.Panel2)
        Me.InfoJoueur.Controls.Add(Me.Panel1)
        Me.InfoJoueur.Location = New System.Drawing.Point(53, 196)
        Me.InfoJoueur.Name = "InfoJoueur"
        Me.InfoJoueur.Size = New System.Drawing.Size(509, 426)
        Me.InfoJoueur.TabIndex = 5
        Me.InfoJoueur.TabStop = False
        Me.InfoJoueur.Text = "InfoJoueur"
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.Label9)
        Me.Panel4.Controls.Add(Me.TB_Age_Joueur4)
        Me.Panel4.Controls.Add(Me.Label4)
        Me.Panel4.Controls.Add(Me.TB_Nom_Joueur4)
        Me.Panel4.Location = New System.Drawing.Point(277, 225)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(209, 188)
        Me.Panel4.TabIndex = 3
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(17, 86)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(42, 20)
        Me.Label9.TabIndex = 6
        Me.Label9.Text = "Age:"
        '
        'TB_Age_Joueur4
        '
        Me.TB_Age_Joueur4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TB_Age_Joueur4.Location = New System.Drawing.Point(19, 108)
        Me.TB_Age_Joueur4.Name = "TB_Age_Joueur4"
        Me.TB_Age_Joueur4.Size = New System.Drawing.Size(157, 29)
        Me.TB_Age_Joueur4.TabIndex = 5
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(15, 18)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(46, 20)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Nom:"
        '
        'TB_Nom_Joueur4
        '
        Me.TB_Nom_Joueur4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TB_Nom_Joueur4.Location = New System.Drawing.Point(19, 40)
        Me.TB_Nom_Joueur4.Name = "TB_Nom_Joueur4"
        Me.TB_Nom_Joueur4.Size = New System.Drawing.Size(157, 29)
        Me.TB_Nom_Joueur4.TabIndex = 3
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Label7)
        Me.Panel3.Controls.Add(Me.TB_Age_Joueur3)
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Controls.Add(Me.TB_Nom_Joueur3)
        Me.Panel3.Location = New System.Drawing.Point(20, 225)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(209, 188)
        Me.Panel3.TabIndex = 2
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(14, 86)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(42, 20)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "Age:"
        '
        'TB_Age_Joueur3
        '
        Me.TB_Age_Joueur3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TB_Age_Joueur3.Location = New System.Drawing.Point(16, 108)
        Me.TB_Age_Joueur3.Name = "TB_Age_Joueur3"
        Me.TB_Age_Joueur3.Size = New System.Drawing.Size(166, 29)
        Me.TB_Age_Joueur3.TabIndex = 4
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(12, 18)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(46, 20)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Nom:"
        '
        'TB_Nom_Joueur3
        '
        Me.TB_Nom_Joueur3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TB_Nom_Joueur3.Location = New System.Drawing.Point(14, 40)
        Me.TB_Nom_Joueur3.Name = "TB_Nom_Joueur3"
        Me.TB_Nom_Joueur3.Size = New System.Drawing.Size(168, 29)
        Me.TB_Nom_Joueur3.TabIndex = 2
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Label8)
        Me.Panel2.Controls.Add(Me.TB_Age_Joueur2)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.TB_Nom_Joueur2)
        Me.Panel2.Location = New System.Drawing.Point(277, 19)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(209, 188)
        Me.Panel2.TabIndex = 1
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(17, 93)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(42, 20)
        Me.Label8.TabIndex = 5
        Me.Label8.Text = "Age:"
        '
        'TB_Age_Joueur2
        '
        Me.TB_Age_Joueur2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TB_Age_Joueur2.Location = New System.Drawing.Point(19, 115)
        Me.TB_Age_Joueur2.Name = "TB_Age_Joueur2"
        Me.TB_Age_Joueur2.Size = New System.Drawing.Size(157, 29)
        Me.TB_Age_Joueur2.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(15, 13)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 20)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Nom:"
        '
        'TB_Nom_Joueur2
        '
        Me.TB_Nom_Joueur2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TB_Nom_Joueur2.Location = New System.Drawing.Point(19, 35)
        Me.TB_Nom_Joueur2.Name = "TB_Nom_Joueur2"
        Me.TB_Nom_Joueur2.Size = New System.Drawing.Size(157, 29)
        Me.TB_Nom_Joueur2.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.TB_Age_Joueur1)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.TB_Nom_Joueur1)
        Me.Panel1.Location = New System.Drawing.Point(20, 19)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(209, 188)
        Me.Panel1.TabIndex = 0
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(14, 93)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(42, 20)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Age:"
        '
        'TB_Age_Joueur1
        '
        Me.TB_Age_Joueur1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TB_Age_Joueur1.Location = New System.Drawing.Point(16, 115)
        Me.TB_Age_Joueur1.Name = "TB_Age_Joueur1"
        Me.TB_Age_Joueur1.Size = New System.Drawing.Size(166, 29)
        Me.TB_Age_Joueur1.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 13)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 20)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Nom:"
        '
        'TB_Nom_Joueur1
        '
        Me.TB_Nom_Joueur1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TB_Nom_Joueur1.Location = New System.Drawing.Point(14, 35)
        Me.TB_Nom_Joueur1.Name = "TB_Nom_Joueur1"
        Me.TB_Nom_Joueur1.Size = New System.Drawing.Size(168, 29)
        Me.TB_Nom_Joueur1.TabIndex = 0
        '
        'Bouton_Règles
        '
        Me.Bouton_Règles.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Bouton_Règles.Location = New System.Drawing.Point(36, 735)
        Me.Bouton_Règles.Name = "Bouton_Règles"
        Me.Bouton_Règles.Size = New System.Drawing.Size(130, 36)
        Me.Bouton_Règles.TabIndex = 6
        Me.Bouton_Règles.Text = "Règles"
        Me.Bouton_Règles.UseVisualStyleBackColor = True
        '
        'buttonRegles
        '
        Me.buttonRegles.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.buttonRegles.Location = New System.Drawing.Point(238, 735)
        Me.buttonRegles.Name = "buttonRegles"
        Me.buttonRegles.Size = New System.Drawing.Size(130, 36)
        Me.buttonRegles.TabIndex = 7
        Me.buttonRegles.Text = "Scores"
        Me.buttonRegles.UseVisualStyleBackColor = True
        '
        'Bouton_Quitter
        '
        Me.Bouton_Quitter.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Bouton_Quitter.Location = New System.Drawing.Point(432, 735)
        Me.Bouton_Quitter.Name = "Bouton_Quitter"
        Me.Bouton_Quitter.Size = New System.Drawing.Size(130, 36)
        Me.Bouton_Quitter.TabIndex = 8
        Me.Bouton_Quitter.Text = "Quitter"
        Me.Bouton_Quitter.UseVisualStyleBackColor = True
        '
        'Bouton_Selection2j
        '
        Me.Bouton_Selection2j.Appearance = System.Windows.Forms.Appearance.Button
        Me.Bouton_Selection2j.AutoSize = True
        Me.Bouton_Selection2j.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Bouton_Selection2j.Location = New System.Drawing.Point(57, 124)
        Me.Bouton_Selection2j.Name = "Bouton_Selection2j"
        Me.Bouton_Selection2j.Size = New System.Drawing.Size(89, 30)
        Me.Bouton_Selection2j.TabIndex = 9
        Me.Bouton_Selection2j.TabStop = True
        Me.Bouton_Selection2j.Text = "2 Joueurs"
        Me.Bouton_Selection2j.UseVisualStyleBackColor = True
        '
        'Bouton_Selection3j
        '
        Me.Bouton_Selection3j.Appearance = System.Windows.Forms.Appearance.Button
        Me.Bouton_Selection3j.AutoSize = True
        Me.Bouton_Selection3j.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Bouton_Selection3j.Location = New System.Drawing.Point(250, 124)
        Me.Bouton_Selection3j.Name = "Bouton_Selection3j"
        Me.Bouton_Selection3j.Size = New System.Drawing.Size(89, 30)
        Me.Bouton_Selection3j.TabIndex = 10
        Me.Bouton_Selection3j.TabStop = True
        Me.Bouton_Selection3j.Text = "3 Joueurs"
        Me.Bouton_Selection3j.UseVisualStyleBackColor = True
        '
        'Bouton_Selection4j
        '
        Me.Bouton_Selection4j.Appearance = System.Windows.Forms.Appearance.Button
        Me.Bouton_Selection4j.AutoSize = True
        Me.Bouton_Selection4j.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Bouton_Selection4j.Location = New System.Drawing.Point(453, 124)
        Me.Bouton_Selection4j.Name = "Bouton_Selection4j"
        Me.Bouton_Selection4j.Size = New System.Drawing.Size(89, 30)
        Me.Bouton_Selection4j.TabIndex = 11
        Me.Bouton_Selection4j.TabStop = True
        Me.Bouton_Selection4j.Text = "4 Joueurs"
        Me.Bouton_Selection4j.UseVisualStyleBackColor = True
        '
        'Menu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(614, 795)
        Me.Controls.Add(Me.Bouton_Selection4j)
        Me.Controls.Add(Me.Bouton_Selection3j)
        Me.Controls.Add(Me.Bouton_Selection2j)
        Me.Controls.Add(Me.Bouton_Quitter)
        Me.Controls.Add(Me.buttonRegles)
        Me.Controls.Add(Me.Bouton_Règles)
        Me.Controls.Add(Me.InfoJoueur)
        Me.Controls.Add(Me.buttonJouer)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Menu"
        Me.Text = "Menu Qwirkle"
        Me.InfoJoueur.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents buttonJouer As Button
    Friend WithEvents InfoJoueur As GroupBox
    Friend WithEvents Panel4 As Panel
    Friend WithEvents Label9 As Label
    Friend WithEvents TB_Age_Joueur4 As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents TB_Nom_Joueur4 As TextBox
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Label7 As Label
    Friend WithEvents TB_Age_Joueur3 As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents TB_Nom_Joueur3 As TextBox
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Label8 As Label
    Friend WithEvents TB_Age_Joueur2 As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents TB_Nom_Joueur2 As TextBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label6 As Label
    Friend WithEvents TB_Age_Joueur1 As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents TB_Nom_Joueur1 As TextBox
    Friend WithEvents Bouton_Règles As Button
    Friend WithEvents buttonRegles As Button
    Friend WithEvents Bouton_Quitter As Button
    Friend WithEvents Bouton_Selection2j As RadioButton
    Friend WithEvents Bouton_Selection3j As RadioButton
    Friend WithEvents Bouton_Selection4j As RadioButton
End Class
