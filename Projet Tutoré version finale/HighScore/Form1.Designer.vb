﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Highscore
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Highscore))
        Me.Bouton_OK = New System.Windows.Forms.Button()
        Me.GB_highscore = New System.Windows.Forms.GroupBox()
        Me.GB_1er = New System.Windows.Forms.GroupBox()
        Me.label_date = New System.Windows.Forms.Label()
        Me.label_score = New System.Windows.Forms.Label()
        Me.label_nomjoueur = New System.Windows.Forms.Label()
        Me.Panel_Scrollbar = New System.Windows.Forms.Panel()
        Me.Envoyer_Score = New System.Windows.Forms.Button()
        Me.GB_highscore.SuspendLayout()
        Me.GB_1er.SuspendLayout()
        Me.Panel_Scrollbar.SuspendLayout()
        Me.SuspendLayout()
        '
        'Bouton_OK
        '
        Me.Bouton_OK.Location = New System.Drawing.Point(280, 439)
        Me.Bouton_OK.Name = "Bouton_OK"
        Me.Bouton_OK.Size = New System.Drawing.Size(179, 23)
        Me.Bouton_OK.TabIndex = 1
        Me.Bouton_OK.Text = "OK"
        Me.Bouton_OK.UseVisualStyleBackColor = True
        '
        'GB_highscore
        '
        Me.GB_highscore.BackColor = System.Drawing.Color.Transparent
        Me.GB_highscore.Controls.Add(Me.GB_1er)
        Me.GB_highscore.Location = New System.Drawing.Point(94, 3)
        Me.GB_highscore.Name = "GB_highscore"
        Me.GB_highscore.Size = New System.Drawing.Size(535, 352)
        Me.GB_highscore.TabIndex = 0
        Me.GB_highscore.TabStop = False
        Me.GB_highscore.Text = "Highscore"
        '
        'GB_1er
        '
        Me.GB_1er.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.GB_1er.Controls.Add(Me.label_date)
        Me.GB_1er.Controls.Add(Me.label_score)
        Me.GB_1er.Controls.Add(Me.label_nomjoueur)
        Me.GB_1er.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GB_1er.Location = New System.Drawing.Point(6, 19)
        Me.GB_1er.Name = "GB_1er"
        Me.GB_1er.Size = New System.Drawing.Size(505, 70)
        Me.GB_1er.TabIndex = 0
        Me.GB_1er.TabStop = False
        Me.GB_1er.Text = "1#"
        '
        'label_date
        '
        Me.label_date.AutoSize = True
        Me.label_date.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_date.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.label_date.Location = New System.Drawing.Point(399, 31)
        Me.label_date.Name = "label_date"
        Me.label_date.Size = New System.Drawing.Size(41, 16)
        Me.label_date.TabIndex = 4
        Me.label_date.Text = "Date"
        '
        'label_score
        '
        Me.label_score.AutoSize = True
        Me.label_score.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_score.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.label_score.Location = New System.Drawing.Point(232, 31)
        Me.label_score.Name = "label_score"
        Me.label_score.Size = New System.Drawing.Size(49, 16)
        Me.label_score.TabIndex = 3
        Me.label_score.Text = "Score"
        '
        'label_nomjoueur
        '
        Me.label_nomjoueur.AutoSize = True
        Me.label_nomjoueur.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_nomjoueur.ForeColor = System.Drawing.Color.DimGray
        Me.label_nomjoueur.Location = New System.Drawing.Point(34, 31)
        Me.label_nomjoueur.Name = "label_nomjoueur"
        Me.label_nomjoueur.Size = New System.Drawing.Size(87, 16)
        Me.label_nomjoueur.TabIndex = 2
        Me.label_nomjoueur.Text = "NomJoueur"
        '
        'Panel_Scrollbar
        '
        Me.Panel_Scrollbar.AutoScroll = True
        Me.Panel_Scrollbar.BackColor = System.Drawing.Color.Transparent
        Me.Panel_Scrollbar.Controls.Add(Me.GB_highscore)
        Me.Panel_Scrollbar.Location = New System.Drawing.Point(12, 12)
        Me.Panel_Scrollbar.Name = "Panel_Scrollbar"
        Me.Panel_Scrollbar.Size = New System.Drawing.Size(714, 409)
        Me.Panel_Scrollbar.TabIndex = 2
        Me.Panel_Scrollbar.Visible = False
        '
        'Envoyer_Score
        '
        Me.Envoyer_Score.Location = New System.Drawing.Point(575, 439)
        Me.Envoyer_Score.Name = "Envoyer_Score"
        Me.Envoyer_Score.Size = New System.Drawing.Size(151, 23)
        Me.Envoyer_Score.TabIndex = 3
        Me.Envoyer_Score.Text = "Envoyer score"
        Me.Envoyer_Score.UseVisualStyleBackColor = True
        '
        'Highscore
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(738, 492)
        Me.Controls.Add(Me.Envoyer_Score)
        Me.Controls.Add(Me.Panel_Scrollbar)
        Me.Controls.Add(Me.Bouton_OK)
        Me.Name = "Highscore"
        Me.Text = "Fenêtre Highscore"
        Me.GB_highscore.ResumeLayout(False)
        Me.GB_1er.ResumeLayout(False)
        Me.GB_1er.PerformLayout()
        Me.Panel_Scrollbar.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Bouton_OK As Button
    Friend WithEvents GB_highscore As GroupBox
    Friend WithEvents GB_1er As GroupBox
    Friend WithEvents label_date As Label
    Friend WithEvents label_score As Label
    Friend WithEvents label_nomjoueur As Label
    Friend WithEvents Panel_Scrollbar As Panel
    Friend WithEvents Envoyer_Score As Button
End Class
