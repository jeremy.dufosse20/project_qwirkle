﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class jeuForm
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(jeuForm))
        Me.btnMenu = New System.Windows.Forms.Button()
        Me.labelQwirkle = New System.Windows.Forms.Label()
        Me.grpboxJoueurs = New System.Windows.Forms.GroupBox()
        Me.labelScore4 = New System.Windows.Forms.Label()
        Me.labelScore3 = New System.Windows.Forms.Label()
        Me.labelScore2 = New System.Windows.Forms.Label()
        Me.labelScore1 = New System.Windows.Forms.Label()
        Me.labelJoueur4 = New System.Windows.Forms.Label()
        Me.labelJoueur3 = New System.Windows.Forms.Label()
        Me.labelJoueur2 = New System.Windows.Forms.Label()
        Me.labelJoueur1 = New System.Windows.Forms.Label()
        Me.labelNombreTuilesRest = New System.Windows.Forms.Label()
        Me.labelTuilesRestantes = New System.Windows.Forms.Label()
        Me.btnFindePartie = New System.Windows.Forms.Button()
        Me.btnRefuse = New System.Windows.Forms.Button()
        Me.btnAccept = New System.Windows.Forms.Button()
        Me.pctBoxPioche0 = New System.Windows.Forms.PictureBox()
        Me.pctBoxPioche1 = New System.Windows.Forms.PictureBox()
        Me.pctBoxPioche2 = New System.Windows.Forms.PictureBox()
        Me.pctBoxPioche3 = New System.Windows.Forms.PictureBox()
        Me.pctBoxPioche4 = New System.Windows.Forms.PictureBox()
        Me.pctBoxPioche5 = New System.Windows.Forms.PictureBox()
        Me.grpBoxScore = New System.Windows.Forms.GroupBox()
        Me.grpBoxTuiles = New System.Windows.Forms.GroupBox()
        Me.grpboxJoueurs.SuspendLayout()
        CType(Me.pctBoxPioche0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pctBoxPioche1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pctBoxPioche2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pctBoxPioche3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pctBoxPioche4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pctBoxPioche5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpBoxScore.SuspendLayout()
        Me.grpBoxTuiles.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnMenu
        '
        Me.btnMenu.Location = New System.Drawing.Point(28, 27)
        Me.btnMenu.Name = "btnMenu"
        Me.btnMenu.Size = New System.Drawing.Size(75, 23)
        Me.btnMenu.TabIndex = 0
        Me.btnMenu.Text = "< Menu"
        Me.btnMenu.UseVisualStyleBackColor = True
        '
        'labelQwirkle
        '
        Me.labelQwirkle.AutoSize = True
        Me.labelQwirkle.Font = New System.Drawing.Font("Microsoft Sans Serif", 28.0!)
        Me.labelQwirkle.Location = New System.Drawing.Point(446, 9)
        Me.labelQwirkle.Name = "labelQwirkle"
        Me.labelQwirkle.Size = New System.Drawing.Size(148, 44)
        Me.labelQwirkle.TabIndex = 1
        Me.labelQwirkle.Text = "Qwirkle"
        '
        'grpboxJoueurs
        '
        Me.grpboxJoueurs.Controls.Add(Me.labelScore4)
        Me.grpboxJoueurs.Controls.Add(Me.labelScore3)
        Me.grpboxJoueurs.Controls.Add(Me.labelScore2)
        Me.grpboxJoueurs.Controls.Add(Me.labelScore1)
        Me.grpboxJoueurs.Controls.Add(Me.labelJoueur4)
        Me.grpboxJoueurs.Controls.Add(Me.labelJoueur3)
        Me.grpboxJoueurs.Controls.Add(Me.labelJoueur2)
        Me.grpboxJoueurs.Controls.Add(Me.labelJoueur1)
        Me.grpboxJoueurs.Location = New System.Drawing.Point(38, 28)
        Me.grpboxJoueurs.Name = "grpboxJoueurs"
        Me.grpboxJoueurs.Size = New System.Drawing.Size(120, 176)
        Me.grpboxJoueurs.TabIndex = 2
        Me.grpboxJoueurs.TabStop = False
        Me.grpboxJoueurs.Text = "Score"
        '
        'labelScore4
        '
        Me.labelScore4.AutoSize = True
        Me.labelScore4.Location = New System.Drawing.Point(70, 137)
        Me.labelScore4.Name = "labelScore4"
        Me.labelScore4.Size = New System.Drawing.Size(13, 13)
        Me.labelScore4.TabIndex = 7
        Me.labelScore4.Text = "0"
        '
        'labelScore3
        '
        Me.labelScore3.AutoSize = True
        Me.labelScore3.Location = New System.Drawing.Point(70, 98)
        Me.labelScore3.Name = "labelScore3"
        Me.labelScore3.Size = New System.Drawing.Size(13, 13)
        Me.labelScore3.TabIndex = 6
        Me.labelScore3.Text = "0"
        '
        'labelScore2
        '
        Me.labelScore2.AutoSize = True
        Me.labelScore2.Location = New System.Drawing.Point(70, 61)
        Me.labelScore2.Name = "labelScore2"
        Me.labelScore2.Size = New System.Drawing.Size(13, 13)
        Me.labelScore2.TabIndex = 5
        Me.labelScore2.Text = "0"
        '
        'labelScore1
        '
        Me.labelScore1.AutoSize = True
        Me.labelScore1.Location = New System.Drawing.Point(70, 25)
        Me.labelScore1.Name = "labelScore1"
        Me.labelScore1.Size = New System.Drawing.Size(13, 13)
        Me.labelScore1.TabIndex = 4
        Me.labelScore1.Text = "0"
        '
        'labelJoueur4
        '
        Me.labelJoueur4.AutoSize = True
        Me.labelJoueur4.Location = New System.Drawing.Point(7, 137)
        Me.labelJoueur4.Name = "labelJoueur4"
        Me.labelJoueur4.Size = New System.Drawing.Size(54, 13)
        Me.labelJoueur4.TabIndex = 3
        Me.labelJoueur4.Text = "Joueur 4 :"
        '
        'labelJoueur3
        '
        Me.labelJoueur3.AutoSize = True
        Me.labelJoueur3.Location = New System.Drawing.Point(7, 98)
        Me.labelJoueur3.Name = "labelJoueur3"
        Me.labelJoueur3.Size = New System.Drawing.Size(54, 13)
        Me.labelJoueur3.TabIndex = 2
        Me.labelJoueur3.Text = "Joueur 3 :"
        '
        'labelJoueur2
        '
        Me.labelJoueur2.AutoSize = True
        Me.labelJoueur2.Location = New System.Drawing.Point(7, 61)
        Me.labelJoueur2.Name = "labelJoueur2"
        Me.labelJoueur2.Size = New System.Drawing.Size(54, 13)
        Me.labelJoueur2.TabIndex = 1
        Me.labelJoueur2.Text = "Joueur 2 :"
        '
        'labelJoueur1
        '
        Me.labelJoueur1.AutoSize = True
        Me.labelJoueur1.Location = New System.Drawing.Point(7, 25)
        Me.labelJoueur1.Name = "labelJoueur1"
        Me.labelJoueur1.Size = New System.Drawing.Size(57, 13)
        Me.labelJoueur1.TabIndex = 0
        Me.labelJoueur1.Text = "Joueur 1 : "
        '
        'labelNombreTuilesRest
        '
        Me.labelNombreTuilesRest.AutoSize = True
        Me.labelNombreTuilesRest.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.labelNombreTuilesRest.Location = New System.Drawing.Point(35, 257)
        Me.labelNombreTuilesRest.Name = "labelNombreTuilesRest"
        Me.labelNombreTuilesRest.Size = New System.Drawing.Size(16, 17)
        Me.labelNombreTuilesRest.TabIndex = 3
        Me.labelNombreTuilesRest.Text = "0"
        '
        'labelTuilesRestantes
        '
        Me.labelTuilesRestantes.AutoSize = True
        Me.labelTuilesRestantes.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.labelTuilesRestantes.Location = New System.Drawing.Point(54, 257)
        Me.labelTuilesRestantes.Name = "labelTuilesRestantes"
        Me.labelTuilesRestantes.Size = New System.Drawing.Size(104, 17)
        Me.labelTuilesRestantes.TabIndex = 4
        Me.labelTuilesRestantes.Text = "tuiles restantes"
        '
        'btnFindePartie
        '
        Me.btnFindePartie.Location = New System.Drawing.Point(38, 308)
        Me.btnFindePartie.Name = "btnFindePartie"
        Me.btnFindePartie.Size = New System.Drawing.Size(120, 23)
        Me.btnFindePartie.TabIndex = 5
        Me.btnFindePartie.Text = "Fin de la partie"
        Me.btnFindePartie.UseVisualStyleBackColor = True
        '
        'btnRefuse
        '
        Me.btnRefuse.AllowDrop = True
        Me.btnRefuse.Image = CType(resources.GetObject("btnRefuse.Image"), System.Drawing.Image)
        Me.btnRefuse.Location = New System.Drawing.Point(625, 40)
        Me.btnRefuse.Name = "btnRefuse"
        Me.btnRefuse.Size = New System.Drawing.Size(75, 29)
        Me.btnRefuse.TabIndex = 16
        Me.btnRefuse.UseVisualStyleBackColor = True
        '
        'btnAccept
        '
        Me.btnAccept.Image = CType(resources.GetObject("btnAccept.Image"), System.Drawing.Image)
        Me.btnAccept.Location = New System.Drawing.Point(441, 40)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(78, 29)
        Me.btnAccept.TabIndex = 17
        Me.btnAccept.UseVisualStyleBackColor = True
        '
        'pctBoxPioche0
        '
        Me.pctBoxPioche0.BackColor = System.Drawing.SystemColors.Control
        Me.pctBoxPioche0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pctBoxPioche0.Location = New System.Drawing.Point(16, 30)
        Me.pctBoxPioche0.Name = "pctBoxPioche0"
        Me.pctBoxPioche0.Size = New System.Drawing.Size(51, 52)
        Me.pctBoxPioche0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pctBoxPioche0.TabIndex = 114
        Me.pctBoxPioche0.TabStop = False
        '
        'pctBoxPioche1
        '
        Me.pctBoxPioche1.BackColor = System.Drawing.SystemColors.Control
        Me.pctBoxPioche1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pctBoxPioche1.Location = New System.Drawing.Point(73, 30)
        Me.pctBoxPioche1.Name = "pctBoxPioche1"
        Me.pctBoxPioche1.Size = New System.Drawing.Size(51, 52)
        Me.pctBoxPioche1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pctBoxPioche1.TabIndex = 115
        Me.pctBoxPioche1.TabStop = False
        '
        'pctBoxPioche2
        '
        Me.pctBoxPioche2.BackColor = System.Drawing.SystemColors.Control
        Me.pctBoxPioche2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pctBoxPioche2.Location = New System.Drawing.Point(130, 30)
        Me.pctBoxPioche2.Name = "pctBoxPioche2"
        Me.pctBoxPioche2.Size = New System.Drawing.Size(51, 52)
        Me.pctBoxPioche2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pctBoxPioche2.TabIndex = 116
        Me.pctBoxPioche2.TabStop = False
        '
        'pctBoxPioche3
        '
        Me.pctBoxPioche3.BackColor = System.Drawing.SystemColors.Control
        Me.pctBoxPioche3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pctBoxPioche3.Location = New System.Drawing.Point(187, 30)
        Me.pctBoxPioche3.Name = "pctBoxPioche3"
        Me.pctBoxPioche3.Size = New System.Drawing.Size(51, 52)
        Me.pctBoxPioche3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pctBoxPioche3.TabIndex = 117
        Me.pctBoxPioche3.TabStop = False
        '
        'pctBoxPioche4
        '
        Me.pctBoxPioche4.BackColor = System.Drawing.SystemColors.Control
        Me.pctBoxPioche4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pctBoxPioche4.Location = New System.Drawing.Point(244, 30)
        Me.pctBoxPioche4.Name = "pctBoxPioche4"
        Me.pctBoxPioche4.Size = New System.Drawing.Size(51, 52)
        Me.pctBoxPioche4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pctBoxPioche4.TabIndex = 118
        Me.pctBoxPioche4.TabStop = False
        '
        'pctBoxPioche5
        '
        Me.pctBoxPioche5.BackColor = System.Drawing.SystemColors.Control
        Me.pctBoxPioche5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pctBoxPioche5.Location = New System.Drawing.Point(301, 30)
        Me.pctBoxPioche5.Name = "pctBoxPioche5"
        Me.pctBoxPioche5.Size = New System.Drawing.Size(51, 52)
        Me.pctBoxPioche5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pctBoxPioche5.TabIndex = 119
        Me.pctBoxPioche5.TabStop = False
        '
        'grpBoxScore
        '
        Me.grpBoxScore.Controls.Add(Me.grpboxJoueurs)
        Me.grpBoxScore.Controls.Add(Me.labelTuilesRestantes)
        Me.grpBoxScore.Controls.Add(Me.labelNombreTuilesRest)
        Me.grpBoxScore.Controls.Add(Me.btnFindePartie)
        Me.grpBoxScore.Location = New System.Drawing.Point(811, 79)
        Me.grpBoxScore.Name = "grpBoxScore"
        Me.grpBoxScore.Size = New System.Drawing.Size(203, 364)
        Me.grpBoxScore.TabIndex = 120
        Me.grpBoxScore.TabStop = False
        '
        'grpBoxTuiles
        '
        Me.grpBoxTuiles.Controls.Add(Me.pctBoxPioche0)
        Me.grpBoxTuiles.Controls.Add(Me.btnRefuse)
        Me.grpBoxTuiles.Controls.Add(Me.pctBoxPioche5)
        Me.grpBoxTuiles.Controls.Add(Me.btnAccept)
        Me.grpBoxTuiles.Controls.Add(Me.pctBoxPioche4)
        Me.grpBoxTuiles.Controls.Add(Me.pctBoxPioche1)
        Me.grpBoxTuiles.Controls.Add(Me.pctBoxPioche3)
        Me.grpBoxTuiles.Controls.Add(Me.pctBoxPioche2)
        Me.grpBoxTuiles.Location = New System.Drawing.Point(12, 770)
        Me.grpBoxTuiles.Name = "grpBoxTuiles"
        Me.grpBoxTuiles.Size = New System.Drawing.Size(718, 100)
        Me.grpBoxTuiles.TabIndex = 121
        Me.grpBoxTuiles.TabStop = False
        '
        'jeuForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1026, 882)
        Me.Controls.Add(Me.grpBoxTuiles)
        Me.Controls.Add(Me.grpBoxScore)
        Me.Controls.Add(Me.labelQwirkle)
        Me.Controls.Add(Me.btnMenu)
        Me.Name = "jeuForm"
        Me.Text = "Form1"
        Me.grpboxJoueurs.ResumeLayout(False)
        Me.grpboxJoueurs.PerformLayout()
        CType(Me.pctBoxPioche0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pctBoxPioche1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pctBoxPioche2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pctBoxPioche3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pctBoxPioche4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pctBoxPioche5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpBoxScore.ResumeLayout(False)
        Me.grpBoxScore.PerformLayout()
        Me.grpBoxTuiles.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnMenu As Button
    Friend WithEvents labelQwirkle As Label
    Friend WithEvents grpboxJoueurs As GroupBox
    Friend WithEvents labelScore4 As Label
    Friend WithEvents labelScore3 As Label
    Friend WithEvents labelScore2 As Label
    Friend WithEvents labelScore1 As Label
    Friend WithEvents labelJoueur4 As Label
    Friend WithEvents labelJoueur3 As Label
    Friend WithEvents labelJoueur2 As Label
    Friend WithEvents labelJoueur1 As Label
    Friend WithEvents labelNombreTuilesRest As Label
    Friend WithEvents labelTuilesRestantes As Label
    Friend WithEvents btnFindePartie As Button
    Friend WithEvents btnRefuse As Button
    Friend WithEvents btnAccept As Button
    Friend WithEvents pctBoxPioche0 As PictureBox
    Friend WithEvents pctBoxPioche1 As PictureBox
    Friend WithEvents pctBoxPioche2 As PictureBox
    Friend WithEvents pctBoxPioche3 As PictureBox
    Friend WithEvents pctBoxPioche4 As PictureBox
    Friend WithEvents pctBoxPioche5 As PictureBox
    Friend WithEvents grpBoxScore As GroupBox
    Friend WithEvents grpBoxTuiles As GroupBox
End Class
