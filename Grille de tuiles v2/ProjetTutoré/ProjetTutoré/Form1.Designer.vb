﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.btnMenu = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.grpboxJoueurs = New System.Windows.Forms.GroupBox()
        Me.labelScore4 = New System.Windows.Forms.Label()
        Me.labelScore3 = New System.Windows.Forms.Label()
        Me.labelScore2 = New System.Windows.Forms.Label()
        Me.labelScore1 = New System.Windows.Forms.Label()
        Me.labelJoueur4 = New System.Windows.Forms.Label()
        Me.labelJoueur3 = New System.Windows.Forms.Label()
        Me.labelJoueur2 = New System.Windows.Forms.Label()
        Me.labelJoueur1 = New System.Windows.Forms.Label()
        Me.labelNombreTuilesRest = New System.Windows.Forms.Label()
        Me.labelTuilesRestantes = New System.Windows.Forms.Label()
        Me.btnFindePartie = New System.Windows.Forms.Button()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PictureBox100 = New System.Windows.Forms.PictureBox()
        Me.PictureBox101 = New System.Windows.Forms.PictureBox()
        Me.PictureBox102 = New System.Windows.Forms.PictureBox()
        Me.PictureBox103 = New System.Windows.Forms.PictureBox()
        Me.PictureBox104 = New System.Windows.Forms.PictureBox()
        Me.PictureBox105 = New System.Windows.Forms.PictureBox()
        Me.PictureBox99 = New System.Windows.Forms.PictureBox()
        Me.PictureBox88 = New System.Windows.Forms.PictureBox()
        Me.PictureBox89 = New System.Windows.Forms.PictureBox()
        Me.PictureBox90 = New System.Windows.Forms.PictureBox()
        Me.PictureBox91 = New System.Windows.Forms.PictureBox()
        Me.PictureBox92 = New System.Windows.Forms.PictureBox()
        Me.PictureBox93 = New System.Windows.Forms.PictureBox()
        Me.PictureBox94 = New System.Windows.Forms.PictureBox()
        Me.PictureBox95 = New System.Windows.Forms.PictureBox()
        Me.PictureBox96 = New System.Windows.Forms.PictureBox()
        Me.PictureBox97 = New System.Windows.Forms.PictureBox()
        Me.PictureBox98 = New System.Windows.Forms.PictureBox()
        Me.PictureBox77 = New System.Windows.Forms.PictureBox()
        Me.PictureBox78 = New System.Windows.Forms.PictureBox()
        Me.PictureBox79 = New System.Windows.Forms.PictureBox()
        Me.PictureBox80 = New System.Windows.Forms.PictureBox()
        Me.PictureBox81 = New System.Windows.Forms.PictureBox()
        Me.PictureBox82 = New System.Windows.Forms.PictureBox()
        Me.PictureBox83 = New System.Windows.Forms.PictureBox()
        Me.PictureBox84 = New System.Windows.Forms.PictureBox()
        Me.PictureBox85 = New System.Windows.Forms.PictureBox()
        Me.PictureBox86 = New System.Windows.Forms.PictureBox()
        Me.PictureBox87 = New System.Windows.Forms.PictureBox()
        Me.PictureBox66 = New System.Windows.Forms.PictureBox()
        Me.PictureBox67 = New System.Windows.Forms.PictureBox()
        Me.PictureBox68 = New System.Windows.Forms.PictureBox()
        Me.PictureBox69 = New System.Windows.Forms.PictureBox()
        Me.PictureBox70 = New System.Windows.Forms.PictureBox()
        Me.PictureBox71 = New System.Windows.Forms.PictureBox()
        Me.PictureBox72 = New System.Windows.Forms.PictureBox()
        Me.PictureBox73 = New System.Windows.Forms.PictureBox()
        Me.PictureBox74 = New System.Windows.Forms.PictureBox()
        Me.PictureBox75 = New System.Windows.Forms.PictureBox()
        Me.PictureBox76 = New System.Windows.Forms.PictureBox()
        Me.PictureBox55 = New System.Windows.Forms.PictureBox()
        Me.PictureBox56 = New System.Windows.Forms.PictureBox()
        Me.PictureBox57 = New System.Windows.Forms.PictureBox()
        Me.PictureBox58 = New System.Windows.Forms.PictureBox()
        Me.PictureBox59 = New System.Windows.Forms.PictureBox()
        Me.PictureBox60 = New System.Windows.Forms.PictureBox()
        Me.PictureBox61 = New System.Windows.Forms.PictureBox()
        Me.PictureBox62 = New System.Windows.Forms.PictureBox()
        Me.PictureBox63 = New System.Windows.Forms.PictureBox()
        Me.PictureBox64 = New System.Windows.Forms.PictureBox()
        Me.PictureBox65 = New System.Windows.Forms.PictureBox()
        Me.PictureBox44 = New System.Windows.Forms.PictureBox()
        Me.PictureBox45 = New System.Windows.Forms.PictureBox()
        Me.PictureBox46 = New System.Windows.Forms.PictureBox()
        Me.PictureBox47 = New System.Windows.Forms.PictureBox()
        Me.PictureBox48 = New System.Windows.Forms.PictureBox()
        Me.PictureBox49 = New System.Windows.Forms.PictureBox()
        Me.PictureBox50 = New System.Windows.Forms.PictureBox()
        Me.PictureBox51 = New System.Windows.Forms.PictureBox()
        Me.PictureBox52 = New System.Windows.Forms.PictureBox()
        Me.PictureBox53 = New System.Windows.Forms.PictureBox()
        Me.PictureBox54 = New System.Windows.Forms.PictureBox()
        Me.PictureBox33 = New System.Windows.Forms.PictureBox()
        Me.PictureBox34 = New System.Windows.Forms.PictureBox()
        Me.PictureBox35 = New System.Windows.Forms.PictureBox()
        Me.PictureBox36 = New System.Windows.Forms.PictureBox()
        Me.PictureBox37 = New System.Windows.Forms.PictureBox()
        Me.PictureBox38 = New System.Windows.Forms.PictureBox()
        Me.PictureBox39 = New System.Windows.Forms.PictureBox()
        Me.PictureBox40 = New System.Windows.Forms.PictureBox()
        Me.PictureBox41 = New System.Windows.Forms.PictureBox()
        Me.PictureBox42 = New System.Windows.Forms.PictureBox()
        Me.PictureBox43 = New System.Windows.Forms.PictureBox()
        Me.PictureBox27 = New System.Windows.Forms.PictureBox()
        Me.PictureBox28 = New System.Windows.Forms.PictureBox()
        Me.PictureBox29 = New System.Windows.Forms.PictureBox()
        Me.PictureBox30 = New System.Windows.Forms.PictureBox()
        Me.PictureBox31 = New System.Windows.Forms.PictureBox()
        Me.PictureBox32 = New System.Windows.Forms.PictureBox()
        Me.PictureBox21 = New System.Windows.Forms.PictureBox()
        Me.PictureBox22 = New System.Windows.Forms.PictureBox()
        Me.PictureBox23 = New System.Windows.Forms.PictureBox()
        Me.PictureBox24 = New System.Windows.Forms.PictureBox()
        Me.PictureBox25 = New System.Windows.Forms.PictureBox()
        Me.PictureBox26 = New System.Windows.Forms.PictureBox()
        Me.PictureBox20 = New System.Windows.Forms.PictureBox()
        Me.PictureBox19 = New System.Windows.Forms.PictureBox()
        Me.PictureBox18 = New System.Windows.Forms.PictureBox()
        Me.PictureBox17 = New System.Windows.Forms.PictureBox()
        Me.PictureBox16 = New System.Windows.Forms.PictureBox()
        Me.PictureBox15 = New System.Windows.Forms.PictureBox()
        Me.PictureBox14 = New System.Windows.Forms.PictureBox()
        Me.PictureBox13 = New System.Windows.Forms.PictureBox()
        Me.PictureBox12 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox11 = New System.Windows.Forms.PictureBox()
        Me.PictureBox10 = New System.Windows.Forms.PictureBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.PictureBox9 = New System.Windows.Forms.PictureBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.PictureBox106 = New System.Windows.Forms.PictureBox()
        Me.PictureBox107 = New System.Windows.Forms.PictureBox()
        Me.PictureBox108 = New System.Windows.Forms.PictureBox()
        Me.PictureBox109 = New System.Windows.Forms.PictureBox()
        Me.PictureBox110 = New System.Windows.Forms.PictureBox()
        Me.PictureBox111 = New System.Windows.Forms.PictureBox()
        Me.PictureBox112 = New System.Windows.Forms.PictureBox()
        Me.PictureBox113 = New System.Windows.Forms.PictureBox()
        Me.PictureBox114 = New System.Windows.Forms.PictureBox()
        Me.PictureBox115 = New System.Windows.Forms.PictureBox()
        Me.PictureBox116 = New System.Windows.Forms.PictureBox()
        Me.PictureBox117 = New System.Windows.Forms.PictureBox()
        Me.PictureBox118 = New System.Windows.Forms.PictureBox()
        Me.PictureBox119 = New System.Windows.Forms.PictureBox()
        Me.PictureBox120 = New System.Windows.Forms.PictureBox()
        Me.PictureBox121 = New System.Windows.Forms.PictureBox()
        Me.PictureBox122 = New System.Windows.Forms.PictureBox()
        Me.PictureBox123 = New System.Windows.Forms.PictureBox()
        Me.PictureBox124 = New System.Windows.Forms.PictureBox()
        Me.PictureBox125 = New System.Windows.Forms.PictureBox()
        Me.PictureBox126 = New System.Windows.Forms.PictureBox()
        Me.PictureBox127 = New System.Windows.Forms.PictureBox()
        Me.PictureBox128 = New System.Windows.Forms.PictureBox()
        Me.PictureBox129 = New System.Windows.Forms.PictureBox()
        Me.PictureBox130 = New System.Windows.Forms.PictureBox()
        Me.PictureBox131 = New System.Windows.Forms.PictureBox()
        Me.PictureBox132 = New System.Windows.Forms.PictureBox()
        Me.PictureBox133 = New System.Windows.Forms.PictureBox()
        Me.PictureBox134 = New System.Windows.Forms.PictureBox()
        Me.PictureBox135 = New System.Windows.Forms.PictureBox()
        Me.PictureBox136 = New System.Windows.Forms.PictureBox()
        Me.PictureBox137 = New System.Windows.Forms.PictureBox()
        Me.PictureBox138 = New System.Windows.Forms.PictureBox()
        Me.PictureBox139 = New System.Windows.Forms.PictureBox()
        Me.PictureBox140 = New System.Windows.Forms.PictureBox()
        Me.PictureBox141 = New System.Windows.Forms.PictureBox()
        Me.PictureBox142 = New System.Windows.Forms.PictureBox()
        Me.PictureBox143 = New System.Windows.Forms.PictureBox()
        Me.PictureBox144 = New System.Windows.Forms.PictureBox()
        Me.PictureBox145 = New System.Windows.Forms.PictureBox()
        Me.PictureBox146 = New System.Windows.Forms.PictureBox()
        Me.PictureBox147 = New System.Windows.Forms.PictureBox()
        Me.PictureBox148 = New System.Windows.Forms.PictureBox()
        Me.PictureBox149 = New System.Windows.Forms.PictureBox()
        Me.PictureBox150 = New System.Windows.Forms.PictureBox()
        Me.PictureBox151 = New System.Windows.Forms.PictureBox()
        Me.PictureBox152 = New System.Windows.Forms.PictureBox()
        Me.PictureBox153 = New System.Windows.Forms.PictureBox()
        Me.PictureBox154 = New System.Windows.Forms.PictureBox()
        Me.PictureBox155 = New System.Windows.Forms.PictureBox()
        Me.PictureBox156 = New System.Windows.Forms.PictureBox()
        Me.PictureBox157 = New System.Windows.Forms.PictureBox()
        Me.PictureBox158 = New System.Windows.Forms.PictureBox()
        Me.PictureBox159 = New System.Windows.Forms.PictureBox()
        Me.PictureBox160 = New System.Windows.Forms.PictureBox()
        Me.PictureBox161 = New System.Windows.Forms.PictureBox()
        Me.PictureBox162 = New System.Windows.Forms.PictureBox()
        Me.PictureBox163 = New System.Windows.Forms.PictureBox()
        Me.PictureBox164 = New System.Windows.Forms.PictureBox()
        Me.PictureBox165 = New System.Windows.Forms.PictureBox()
        Me.PictureBox166 = New System.Windows.Forms.PictureBox()
        Me.PictureBox167 = New System.Windows.Forms.PictureBox()
        Me.PictureBox168 = New System.Windows.Forms.PictureBox()
        Me.PictureBox169 = New System.Windows.Forms.PictureBox()
        Me.PictureBox170 = New System.Windows.Forms.PictureBox()
        Me.PictureBox171 = New System.Windows.Forms.PictureBox()
        Me.PictureBox172 = New System.Windows.Forms.PictureBox()
        Me.PictureBox173 = New System.Windows.Forms.PictureBox()
        Me.PictureBox174 = New System.Windows.Forms.PictureBox()
        Me.PictureBox175 = New System.Windows.Forms.PictureBox()
        Me.PictureBox176 = New System.Windows.Forms.PictureBox()
        Me.PictureBox177 = New System.Windows.Forms.PictureBox()
        Me.PictureBox178 = New System.Windows.Forms.PictureBox()
        Me.PictureBox179 = New System.Windows.Forms.PictureBox()
        Me.PictureBox180 = New System.Windows.Forms.PictureBox()
        Me.PictureBox181 = New System.Windows.Forms.PictureBox()
        Me.PictureBox182 = New System.Windows.Forms.PictureBox()
        Me.PictureBox183 = New System.Windows.Forms.PictureBox()
        Me.PictureBox184 = New System.Windows.Forms.PictureBox()
        Me.PictureBox185 = New System.Windows.Forms.PictureBox()
        Me.PictureBox186 = New System.Windows.Forms.PictureBox()
        Me.grpboxJoueurs.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox100, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox101, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox102, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox103, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox104, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox105, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox99, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox88, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox89, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox90, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox91, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox92, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox93, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox94, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox95, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox96, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox97, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox98, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox77, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox78, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox79, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox80, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox81, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox82, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox83, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox84, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox85, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox86, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox87, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox66, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox67, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox68, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox69, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox70, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox71, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox72, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox73, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox74, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox75, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox76, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox55, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox56, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox57, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox58, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox59, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox60, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox61, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox62, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox63, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox64, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox65, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox44, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox45, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox46, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox47, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox48, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox49, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox50, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox51, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox52, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox53, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox54, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox38, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox39, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox40, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.PictureBox106, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox107, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox108, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox109, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox110, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox111, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox112, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox113, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox114, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox115, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox116, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox117, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox118, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox119, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox120, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox121, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox122, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox123, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox124, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox125, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox126, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox127, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox128, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox129, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox130, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox131, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox132, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox133, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox134, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox135, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox136, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox137, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox138, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox139, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox140, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox141, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox142, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox143, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox144, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox145, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox146, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox147, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox148, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox149, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox150, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox151, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox152, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox153, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox154, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox155, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox156, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox157, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox158, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox159, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox160, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox161, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox162, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox163, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox164, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox165, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox166, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox167, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox168, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox169, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox170, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox171, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox172, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox173, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox174, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox175, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox176, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox177, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox178, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox179, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox180, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox181, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox182, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox183, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox184, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox185, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox186, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnMenu
        '
        Me.btnMenu.Location = New System.Drawing.Point(28, 27)
        Me.btnMenu.Name = "btnMenu"
        Me.btnMenu.Size = New System.Drawing.Size(75, 23)
        Me.btnMenu.TabIndex = 0
        Me.btnMenu.Text = "< Menu"
        Me.btnMenu.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 28.0!)
        Me.Label1.Location = New System.Drawing.Point(446, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(148, 44)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Qwirkle"
        '
        'grpboxJoueurs
        '
        Me.grpboxJoueurs.Controls.Add(Me.labelScore4)
        Me.grpboxJoueurs.Controls.Add(Me.labelScore3)
        Me.grpboxJoueurs.Controls.Add(Me.labelScore2)
        Me.grpboxJoueurs.Controls.Add(Me.labelScore1)
        Me.grpboxJoueurs.Controls.Add(Me.labelJoueur4)
        Me.grpboxJoueurs.Controls.Add(Me.labelJoueur3)
        Me.grpboxJoueurs.Controls.Add(Me.labelJoueur2)
        Me.grpboxJoueurs.Controls.Add(Me.labelJoueur1)
        Me.grpboxJoueurs.Location = New System.Drawing.Point(847, 50)
        Me.grpboxJoueurs.Name = "grpboxJoueurs"
        Me.grpboxJoueurs.Size = New System.Drawing.Size(120, 176)
        Me.grpboxJoueurs.TabIndex = 2
        Me.grpboxJoueurs.TabStop = False
        Me.grpboxJoueurs.Text = "Score"
        '
        'labelScore4
        '
        Me.labelScore4.AutoSize = True
        Me.labelScore4.Location = New System.Drawing.Point(70, 137)
        Me.labelScore4.Name = "labelScore4"
        Me.labelScore4.Size = New System.Drawing.Size(13, 13)
        Me.labelScore4.TabIndex = 7
        Me.labelScore4.Text = "0"
        '
        'labelScore3
        '
        Me.labelScore3.AutoSize = True
        Me.labelScore3.Location = New System.Drawing.Point(70, 98)
        Me.labelScore3.Name = "labelScore3"
        Me.labelScore3.Size = New System.Drawing.Size(13, 13)
        Me.labelScore3.TabIndex = 6
        Me.labelScore3.Text = "0"
        '
        'labelScore2
        '
        Me.labelScore2.AutoSize = True
        Me.labelScore2.Location = New System.Drawing.Point(70, 61)
        Me.labelScore2.Name = "labelScore2"
        Me.labelScore2.Size = New System.Drawing.Size(13, 13)
        Me.labelScore2.TabIndex = 5
        Me.labelScore2.Text = "0"
        '
        'labelScore1
        '
        Me.labelScore1.AutoSize = True
        Me.labelScore1.Location = New System.Drawing.Point(70, 25)
        Me.labelScore1.Name = "labelScore1"
        Me.labelScore1.Size = New System.Drawing.Size(13, 13)
        Me.labelScore1.TabIndex = 4
        Me.labelScore1.Text = "0"
        '
        'labelJoueur4
        '
        Me.labelJoueur4.AutoSize = True
        Me.labelJoueur4.Location = New System.Drawing.Point(7, 137)
        Me.labelJoueur4.Name = "labelJoueur4"
        Me.labelJoueur4.Size = New System.Drawing.Size(54, 13)
        Me.labelJoueur4.TabIndex = 3
        Me.labelJoueur4.Text = "Joueur 4 :"
        '
        'labelJoueur3
        '
        Me.labelJoueur3.AutoSize = True
        Me.labelJoueur3.Location = New System.Drawing.Point(7, 98)
        Me.labelJoueur3.Name = "labelJoueur3"
        Me.labelJoueur3.Size = New System.Drawing.Size(54, 13)
        Me.labelJoueur3.TabIndex = 2
        Me.labelJoueur3.Text = "Joueur 3 :"
        '
        'labelJoueur2
        '
        Me.labelJoueur2.AutoSize = True
        Me.labelJoueur2.Location = New System.Drawing.Point(7, 61)
        Me.labelJoueur2.Name = "labelJoueur2"
        Me.labelJoueur2.Size = New System.Drawing.Size(54, 13)
        Me.labelJoueur2.TabIndex = 1
        Me.labelJoueur2.Text = "Joueur 2 :"
        '
        'labelJoueur1
        '
        Me.labelJoueur1.AutoSize = True
        Me.labelJoueur1.Location = New System.Drawing.Point(7, 25)
        Me.labelJoueur1.Name = "labelJoueur1"
        Me.labelJoueur1.Size = New System.Drawing.Size(57, 13)
        Me.labelJoueur1.TabIndex = 0
        Me.labelJoueur1.Text = "Joueur 1 : "
        '
        'labelNombreTuilesRest
        '
        Me.labelNombreTuilesRest.AutoSize = True
        Me.labelNombreTuilesRest.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.labelNombreTuilesRest.Location = New System.Drawing.Point(854, 277)
        Me.labelNombreTuilesRest.Name = "labelNombreTuilesRest"
        Me.labelNombreTuilesRest.Size = New System.Drawing.Size(16, 17)
        Me.labelNombreTuilesRest.TabIndex = 3
        Me.labelNombreTuilesRest.Text = "0"
        '
        'labelTuilesRestantes
        '
        Me.labelTuilesRestantes.AutoSize = True
        Me.labelTuilesRestantes.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.labelTuilesRestantes.Location = New System.Drawing.Point(882, 277)
        Me.labelTuilesRestantes.Name = "labelTuilesRestantes"
        Me.labelTuilesRestantes.Size = New System.Drawing.Size(104, 17)
        Me.labelTuilesRestantes.TabIndex = 4
        Me.labelTuilesRestantes.Text = "tuiles restantes"
        '
        'btnFindePartie
        '
        Me.btnFindePartie.Location = New System.Drawing.Point(866, 328)
        Me.btnFindePartie.Name = "btnFindePartie"
        Me.btnFindePartie.Size = New System.Drawing.Size(120, 23)
        Me.btnFindePartie.TabIndex = 5
        Me.btnFindePartie.Text = "Fin de la partie"
        Me.btnFindePartie.UseVisualStyleBackColor = True
        '
        'PictureBox3
        '
        Me.PictureBox3.Location = New System.Drawing.Point(513, 734)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(80, 29)
        Me.PictureBox3.TabIndex = 8
        Me.PictureBox3.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.PictureBox100)
        Me.Panel1.Controls.Add(Me.PictureBox101)
        Me.Panel1.Controls.Add(Me.PictureBox102)
        Me.Panel1.Controls.Add(Me.PictureBox103)
        Me.Panel1.Controls.Add(Me.PictureBox104)
        Me.Panel1.Controls.Add(Me.PictureBox105)
        Me.Panel1.Controls.Add(Me.PictureBox99)
        Me.Panel1.Controls.Add(Me.PictureBox88)
        Me.Panel1.Controls.Add(Me.PictureBox89)
        Me.Panel1.Controls.Add(Me.PictureBox90)
        Me.Panel1.Controls.Add(Me.PictureBox91)
        Me.Panel1.Controls.Add(Me.PictureBox92)
        Me.Panel1.Controls.Add(Me.PictureBox93)
        Me.Panel1.Controls.Add(Me.PictureBox94)
        Me.Panel1.Controls.Add(Me.PictureBox95)
        Me.Panel1.Controls.Add(Me.PictureBox96)
        Me.Panel1.Controls.Add(Me.PictureBox97)
        Me.Panel1.Controls.Add(Me.PictureBox98)
        Me.Panel1.Controls.Add(Me.PictureBox77)
        Me.Panel1.Controls.Add(Me.PictureBox78)
        Me.Panel1.Controls.Add(Me.PictureBox79)
        Me.Panel1.Controls.Add(Me.PictureBox80)
        Me.Panel1.Controls.Add(Me.PictureBox81)
        Me.Panel1.Controls.Add(Me.PictureBox82)
        Me.Panel1.Controls.Add(Me.PictureBox83)
        Me.Panel1.Controls.Add(Me.PictureBox84)
        Me.Panel1.Controls.Add(Me.PictureBox85)
        Me.Panel1.Controls.Add(Me.PictureBox86)
        Me.Panel1.Controls.Add(Me.PictureBox87)
        Me.Panel1.Controls.Add(Me.PictureBox66)
        Me.Panel1.Controls.Add(Me.PictureBox67)
        Me.Panel1.Controls.Add(Me.PictureBox68)
        Me.Panel1.Controls.Add(Me.PictureBox69)
        Me.Panel1.Controls.Add(Me.PictureBox70)
        Me.Panel1.Controls.Add(Me.PictureBox71)
        Me.Panel1.Controls.Add(Me.PictureBox72)
        Me.Panel1.Controls.Add(Me.PictureBox73)
        Me.Panel1.Controls.Add(Me.PictureBox74)
        Me.Panel1.Controls.Add(Me.PictureBox75)
        Me.Panel1.Controls.Add(Me.PictureBox76)
        Me.Panel1.Controls.Add(Me.PictureBox55)
        Me.Panel1.Controls.Add(Me.PictureBox56)
        Me.Panel1.Controls.Add(Me.PictureBox57)
        Me.Panel1.Controls.Add(Me.PictureBox58)
        Me.Panel1.Controls.Add(Me.PictureBox59)
        Me.Panel1.Controls.Add(Me.PictureBox60)
        Me.Panel1.Controls.Add(Me.PictureBox61)
        Me.Panel1.Controls.Add(Me.PictureBox62)
        Me.Panel1.Controls.Add(Me.PictureBox63)
        Me.Panel1.Controls.Add(Me.PictureBox64)
        Me.Panel1.Controls.Add(Me.PictureBox65)
        Me.Panel1.Controls.Add(Me.PictureBox44)
        Me.Panel1.Controls.Add(Me.PictureBox45)
        Me.Panel1.Controls.Add(Me.PictureBox46)
        Me.Panel1.Controls.Add(Me.PictureBox47)
        Me.Panel1.Controls.Add(Me.PictureBox48)
        Me.Panel1.Controls.Add(Me.PictureBox49)
        Me.Panel1.Controls.Add(Me.PictureBox50)
        Me.Panel1.Controls.Add(Me.PictureBox51)
        Me.Panel1.Controls.Add(Me.PictureBox52)
        Me.Panel1.Controls.Add(Me.PictureBox53)
        Me.Panel1.Controls.Add(Me.PictureBox54)
        Me.Panel1.Controls.Add(Me.PictureBox33)
        Me.Panel1.Controls.Add(Me.PictureBox34)
        Me.Panel1.Controls.Add(Me.PictureBox35)
        Me.Panel1.Controls.Add(Me.PictureBox36)
        Me.Panel1.Controls.Add(Me.PictureBox37)
        Me.Panel1.Controls.Add(Me.PictureBox38)
        Me.Panel1.Controls.Add(Me.PictureBox39)
        Me.Panel1.Controls.Add(Me.PictureBox40)
        Me.Panel1.Controls.Add(Me.PictureBox41)
        Me.Panel1.Controls.Add(Me.PictureBox42)
        Me.Panel1.Controls.Add(Me.PictureBox43)
        Me.Panel1.Controls.Add(Me.PictureBox27)
        Me.Panel1.Controls.Add(Me.PictureBox28)
        Me.Panel1.Controls.Add(Me.PictureBox29)
        Me.Panel1.Controls.Add(Me.PictureBox30)
        Me.Panel1.Controls.Add(Me.PictureBox31)
        Me.Panel1.Controls.Add(Me.PictureBox32)
        Me.Panel1.Controls.Add(Me.PictureBox21)
        Me.Panel1.Controls.Add(Me.PictureBox22)
        Me.Panel1.Controls.Add(Me.PictureBox23)
        Me.Panel1.Controls.Add(Me.PictureBox24)
        Me.Panel1.Controls.Add(Me.PictureBox25)
        Me.Panel1.Controls.Add(Me.PictureBox26)
        Me.Panel1.Controls.Add(Me.PictureBox20)
        Me.Panel1.Controls.Add(Me.PictureBox19)
        Me.Panel1.Controls.Add(Me.PictureBox18)
        Me.Panel1.Controls.Add(Me.PictureBox17)
        Me.Panel1.Controls.Add(Me.PictureBox16)
        Me.Panel1.Controls.Add(Me.PictureBox15)
        Me.Panel1.Controls.Add(Me.PictureBox14)
        Me.Panel1.Controls.Add(Me.PictureBox13)
        Me.Panel1.Controls.Add(Me.PictureBox12)
        Me.Panel1.Controls.Add(Me.PictureBox2)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Controls.Add(Me.PictureBox11)
        Me.Panel1.Controls.Add(Me.PictureBox10)
        Me.Panel1.Location = New System.Drawing.Point(28, 75)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(807, 424)
        Me.Panel1.TabIndex = 15
        '
        'PictureBox100
        '
        Me.PictureBox100.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox100.Location = New System.Drawing.Point(744, 351)
        Me.PictureBox100.Name = "PictureBox100"
        Me.PictureBox100.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox100.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox100.TabIndex = 113
        Me.PictureBox100.TabStop = False
        '
        'PictureBox101
        '
        Me.PictureBox101.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox101.Location = New System.Drawing.Point(744, 293)
        Me.PictureBox101.Name = "PictureBox101"
        Me.PictureBox101.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox101.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox101.TabIndex = 112
        Me.PictureBox101.TabStop = False
        '
        'PictureBox102
        '
        Me.PictureBox102.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox102.Location = New System.Drawing.Point(744, 235)
        Me.PictureBox102.Name = "PictureBox102"
        Me.PictureBox102.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox102.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox102.TabIndex = 111
        Me.PictureBox102.TabStop = False
        '
        'PictureBox103
        '
        Me.PictureBox103.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox103.Location = New System.Drawing.Point(744, 177)
        Me.PictureBox103.Name = "PictureBox103"
        Me.PictureBox103.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox103.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox103.TabIndex = 110
        Me.PictureBox103.TabStop = False
        '
        'PictureBox104
        '
        Me.PictureBox104.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox104.Location = New System.Drawing.Point(744, 119)
        Me.PictureBox104.Name = "PictureBox104"
        Me.PictureBox104.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox104.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox104.TabIndex = 109
        Me.PictureBox104.TabStop = False
        '
        'PictureBox105
        '
        Me.PictureBox105.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox105.Location = New System.Drawing.Point(744, 61)
        Me.PictureBox105.Name = "PictureBox105"
        Me.PictureBox105.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox105.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox105.TabIndex = 108
        Me.PictureBox105.TabStop = False
        '
        'PictureBox99
        '
        Me.PictureBox99.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox99.Location = New System.Drawing.Point(744, 3)
        Me.PictureBox99.Name = "PictureBox99"
        Me.PictureBox99.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox99.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox99.TabIndex = 107
        Me.PictureBox99.TabStop = False
        '
        'PictureBox88
        '
        Me.PictureBox88.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox88.Location = New System.Drawing.Point(629, 293)
        Me.PictureBox88.Name = "PictureBox88"
        Me.PictureBox88.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox88.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox88.TabIndex = 106
        Me.PictureBox88.TabStop = False
        '
        'PictureBox89
        '
        Me.PictureBox89.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox89.Location = New System.Drawing.Point(572, 293)
        Me.PictureBox89.Name = "PictureBox89"
        Me.PictureBox89.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox89.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox89.TabIndex = 105
        Me.PictureBox89.TabStop = False
        '
        'PictureBox90
        '
        Me.PictureBox90.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox90.Location = New System.Drawing.Point(515, 293)
        Me.PictureBox90.Name = "PictureBox90"
        Me.PictureBox90.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox90.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox90.TabIndex = 104
        Me.PictureBox90.TabStop = False
        '
        'PictureBox91
        '
        Me.PictureBox91.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox91.Location = New System.Drawing.Point(458, 293)
        Me.PictureBox91.Name = "PictureBox91"
        Me.PictureBox91.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox91.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox91.TabIndex = 103
        Me.PictureBox91.TabStop = False
        '
        'PictureBox92
        '
        Me.PictureBox92.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox92.Location = New System.Drawing.Point(401, 293)
        Me.PictureBox92.Name = "PictureBox92"
        Me.PictureBox92.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox92.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox92.TabIndex = 102
        Me.PictureBox92.TabStop = False
        '
        'PictureBox93
        '
        Me.PictureBox93.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox93.Location = New System.Drawing.Point(344, 293)
        Me.PictureBox93.Name = "PictureBox93"
        Me.PictureBox93.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox93.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox93.TabIndex = 101
        Me.PictureBox93.TabStop = False
        '
        'PictureBox94
        '
        Me.PictureBox94.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox94.Location = New System.Drawing.Point(287, 293)
        Me.PictureBox94.Name = "PictureBox94"
        Me.PictureBox94.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox94.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox94.TabIndex = 100
        Me.PictureBox94.TabStop = False
        '
        'PictureBox95
        '
        Me.PictureBox95.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox95.Location = New System.Drawing.Point(230, 293)
        Me.PictureBox95.Name = "PictureBox95"
        Me.PictureBox95.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox95.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox95.TabIndex = 99
        Me.PictureBox95.TabStop = False
        '
        'PictureBox96
        '
        Me.PictureBox96.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox96.Location = New System.Drawing.Point(173, 293)
        Me.PictureBox96.Name = "PictureBox96"
        Me.PictureBox96.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox96.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox96.TabIndex = 98
        Me.PictureBox96.TabStop = False
        '
        'PictureBox97
        '
        Me.PictureBox97.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox97.Location = New System.Drawing.Point(116, 293)
        Me.PictureBox97.Name = "PictureBox97"
        Me.PictureBox97.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox97.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox97.TabIndex = 97
        Me.PictureBox97.TabStop = False
        '
        'PictureBox98
        '
        Me.PictureBox98.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox98.Location = New System.Drawing.Point(59, 293)
        Me.PictureBox98.Name = "PictureBox98"
        Me.PictureBox98.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox98.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox98.TabIndex = 96
        Me.PictureBox98.TabStop = False
        '
        'PictureBox77
        '
        Me.PictureBox77.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox77.Location = New System.Drawing.Point(630, 235)
        Me.PictureBox77.Name = "PictureBox77"
        Me.PictureBox77.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox77.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox77.TabIndex = 95
        Me.PictureBox77.TabStop = False
        '
        'PictureBox78
        '
        Me.PictureBox78.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox78.Location = New System.Drawing.Point(573, 235)
        Me.PictureBox78.Name = "PictureBox78"
        Me.PictureBox78.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox78.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox78.TabIndex = 94
        Me.PictureBox78.TabStop = False
        '
        'PictureBox79
        '
        Me.PictureBox79.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox79.Location = New System.Drawing.Point(516, 235)
        Me.PictureBox79.Name = "PictureBox79"
        Me.PictureBox79.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox79.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox79.TabIndex = 93
        Me.PictureBox79.TabStop = False
        '
        'PictureBox80
        '
        Me.PictureBox80.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox80.Location = New System.Drawing.Point(459, 235)
        Me.PictureBox80.Name = "PictureBox80"
        Me.PictureBox80.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox80.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox80.TabIndex = 92
        Me.PictureBox80.TabStop = False
        '
        'PictureBox81
        '
        Me.PictureBox81.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox81.Location = New System.Drawing.Point(402, 235)
        Me.PictureBox81.Name = "PictureBox81"
        Me.PictureBox81.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox81.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox81.TabIndex = 91
        Me.PictureBox81.TabStop = False
        '
        'PictureBox82
        '
        Me.PictureBox82.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox82.Location = New System.Drawing.Point(345, 235)
        Me.PictureBox82.Name = "PictureBox82"
        Me.PictureBox82.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox82.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox82.TabIndex = 90
        Me.PictureBox82.TabStop = False
        '
        'PictureBox83
        '
        Me.PictureBox83.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox83.Location = New System.Drawing.Point(288, 235)
        Me.PictureBox83.Name = "PictureBox83"
        Me.PictureBox83.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox83.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox83.TabIndex = 89
        Me.PictureBox83.TabStop = False
        '
        'PictureBox84
        '
        Me.PictureBox84.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox84.Location = New System.Drawing.Point(231, 235)
        Me.PictureBox84.Name = "PictureBox84"
        Me.PictureBox84.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox84.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox84.TabIndex = 88
        Me.PictureBox84.TabStop = False
        '
        'PictureBox85
        '
        Me.PictureBox85.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox85.Location = New System.Drawing.Point(174, 235)
        Me.PictureBox85.Name = "PictureBox85"
        Me.PictureBox85.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox85.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox85.TabIndex = 87
        Me.PictureBox85.TabStop = False
        '
        'PictureBox86
        '
        Me.PictureBox86.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox86.Location = New System.Drawing.Point(117, 235)
        Me.PictureBox86.Name = "PictureBox86"
        Me.PictureBox86.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox86.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox86.TabIndex = 86
        Me.PictureBox86.TabStop = False
        '
        'PictureBox87
        '
        Me.PictureBox87.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox87.Location = New System.Drawing.Point(60, 235)
        Me.PictureBox87.Name = "PictureBox87"
        Me.PictureBox87.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox87.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox87.TabIndex = 85
        Me.PictureBox87.TabStop = False
        '
        'PictureBox66
        '
        Me.PictureBox66.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox66.Location = New System.Drawing.Point(630, 177)
        Me.PictureBox66.Name = "PictureBox66"
        Me.PictureBox66.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox66.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox66.TabIndex = 84
        Me.PictureBox66.TabStop = False
        '
        'PictureBox67
        '
        Me.PictureBox67.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox67.Location = New System.Drawing.Point(573, 177)
        Me.PictureBox67.Name = "PictureBox67"
        Me.PictureBox67.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox67.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox67.TabIndex = 83
        Me.PictureBox67.TabStop = False
        '
        'PictureBox68
        '
        Me.PictureBox68.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox68.Location = New System.Drawing.Point(516, 177)
        Me.PictureBox68.Name = "PictureBox68"
        Me.PictureBox68.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox68.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox68.TabIndex = 82
        Me.PictureBox68.TabStop = False
        '
        'PictureBox69
        '
        Me.PictureBox69.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox69.Location = New System.Drawing.Point(459, 177)
        Me.PictureBox69.Name = "PictureBox69"
        Me.PictureBox69.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox69.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox69.TabIndex = 81
        Me.PictureBox69.TabStop = False
        '
        'PictureBox70
        '
        Me.PictureBox70.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox70.Location = New System.Drawing.Point(402, 177)
        Me.PictureBox70.Name = "PictureBox70"
        Me.PictureBox70.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox70.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox70.TabIndex = 80
        Me.PictureBox70.TabStop = False
        '
        'PictureBox71
        '
        Me.PictureBox71.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox71.Location = New System.Drawing.Point(345, 177)
        Me.PictureBox71.Name = "PictureBox71"
        Me.PictureBox71.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox71.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox71.TabIndex = 79
        Me.PictureBox71.TabStop = False
        '
        'PictureBox72
        '
        Me.PictureBox72.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox72.Location = New System.Drawing.Point(288, 177)
        Me.PictureBox72.Name = "PictureBox72"
        Me.PictureBox72.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox72.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox72.TabIndex = 78
        Me.PictureBox72.TabStop = False
        '
        'PictureBox73
        '
        Me.PictureBox73.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox73.Location = New System.Drawing.Point(231, 177)
        Me.PictureBox73.Name = "PictureBox73"
        Me.PictureBox73.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox73.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox73.TabIndex = 77
        Me.PictureBox73.TabStop = False
        '
        'PictureBox74
        '
        Me.PictureBox74.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox74.Location = New System.Drawing.Point(174, 177)
        Me.PictureBox74.Name = "PictureBox74"
        Me.PictureBox74.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox74.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox74.TabIndex = 76
        Me.PictureBox74.TabStop = False
        '
        'PictureBox75
        '
        Me.PictureBox75.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox75.Location = New System.Drawing.Point(117, 177)
        Me.PictureBox75.Name = "PictureBox75"
        Me.PictureBox75.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox75.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox75.TabIndex = 75
        Me.PictureBox75.TabStop = False
        '
        'PictureBox76
        '
        Me.PictureBox76.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox76.Location = New System.Drawing.Point(60, 177)
        Me.PictureBox76.Name = "PictureBox76"
        Me.PictureBox76.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox76.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox76.TabIndex = 74
        Me.PictureBox76.TabStop = False
        '
        'PictureBox55
        '
        Me.PictureBox55.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox55.Location = New System.Drawing.Point(630, 119)
        Me.PictureBox55.Name = "PictureBox55"
        Me.PictureBox55.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox55.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox55.TabIndex = 73
        Me.PictureBox55.TabStop = False
        '
        'PictureBox56
        '
        Me.PictureBox56.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox56.Location = New System.Drawing.Point(573, 119)
        Me.PictureBox56.Name = "PictureBox56"
        Me.PictureBox56.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox56.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox56.TabIndex = 72
        Me.PictureBox56.TabStop = False
        '
        'PictureBox57
        '
        Me.PictureBox57.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox57.Location = New System.Drawing.Point(516, 119)
        Me.PictureBox57.Name = "PictureBox57"
        Me.PictureBox57.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox57.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox57.TabIndex = 71
        Me.PictureBox57.TabStop = False
        '
        'PictureBox58
        '
        Me.PictureBox58.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox58.Location = New System.Drawing.Point(459, 119)
        Me.PictureBox58.Name = "PictureBox58"
        Me.PictureBox58.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox58.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox58.TabIndex = 70
        Me.PictureBox58.TabStop = False
        '
        'PictureBox59
        '
        Me.PictureBox59.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox59.Location = New System.Drawing.Point(402, 119)
        Me.PictureBox59.Name = "PictureBox59"
        Me.PictureBox59.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox59.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox59.TabIndex = 69
        Me.PictureBox59.TabStop = False
        '
        'PictureBox60
        '
        Me.PictureBox60.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox60.Location = New System.Drawing.Point(345, 119)
        Me.PictureBox60.Name = "PictureBox60"
        Me.PictureBox60.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox60.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox60.TabIndex = 68
        Me.PictureBox60.TabStop = False
        '
        'PictureBox61
        '
        Me.PictureBox61.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox61.Location = New System.Drawing.Point(288, 119)
        Me.PictureBox61.Name = "PictureBox61"
        Me.PictureBox61.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox61.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox61.TabIndex = 67
        Me.PictureBox61.TabStop = False
        '
        'PictureBox62
        '
        Me.PictureBox62.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox62.Location = New System.Drawing.Point(231, 119)
        Me.PictureBox62.Name = "PictureBox62"
        Me.PictureBox62.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox62.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox62.TabIndex = 66
        Me.PictureBox62.TabStop = False
        '
        'PictureBox63
        '
        Me.PictureBox63.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox63.Location = New System.Drawing.Point(174, 119)
        Me.PictureBox63.Name = "PictureBox63"
        Me.PictureBox63.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox63.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox63.TabIndex = 65
        Me.PictureBox63.TabStop = False
        '
        'PictureBox64
        '
        Me.PictureBox64.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox64.Location = New System.Drawing.Point(117, 119)
        Me.PictureBox64.Name = "PictureBox64"
        Me.PictureBox64.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox64.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox64.TabIndex = 64
        Me.PictureBox64.TabStop = False
        '
        'PictureBox65
        '
        Me.PictureBox65.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox65.Location = New System.Drawing.Point(60, 119)
        Me.PictureBox65.Name = "PictureBox65"
        Me.PictureBox65.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox65.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox65.TabIndex = 63
        Me.PictureBox65.TabStop = False
        '
        'PictureBox44
        '
        Me.PictureBox44.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox44.Location = New System.Drawing.Point(630, 61)
        Me.PictureBox44.Name = "PictureBox44"
        Me.PictureBox44.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox44.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox44.TabIndex = 62
        Me.PictureBox44.TabStop = False
        '
        'PictureBox45
        '
        Me.PictureBox45.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox45.Location = New System.Drawing.Point(573, 61)
        Me.PictureBox45.Name = "PictureBox45"
        Me.PictureBox45.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox45.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox45.TabIndex = 61
        Me.PictureBox45.TabStop = False
        '
        'PictureBox46
        '
        Me.PictureBox46.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox46.Location = New System.Drawing.Point(516, 61)
        Me.PictureBox46.Name = "PictureBox46"
        Me.PictureBox46.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox46.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox46.TabIndex = 60
        Me.PictureBox46.TabStop = False
        '
        'PictureBox47
        '
        Me.PictureBox47.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox47.Location = New System.Drawing.Point(459, 61)
        Me.PictureBox47.Name = "PictureBox47"
        Me.PictureBox47.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox47.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox47.TabIndex = 59
        Me.PictureBox47.TabStop = False
        '
        'PictureBox48
        '
        Me.PictureBox48.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox48.Location = New System.Drawing.Point(402, 61)
        Me.PictureBox48.Name = "PictureBox48"
        Me.PictureBox48.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox48.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox48.TabIndex = 58
        Me.PictureBox48.TabStop = False
        '
        'PictureBox49
        '
        Me.PictureBox49.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox49.Location = New System.Drawing.Point(345, 61)
        Me.PictureBox49.Name = "PictureBox49"
        Me.PictureBox49.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox49.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox49.TabIndex = 57
        Me.PictureBox49.TabStop = False
        '
        'PictureBox50
        '
        Me.PictureBox50.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox50.Location = New System.Drawing.Point(288, 61)
        Me.PictureBox50.Name = "PictureBox50"
        Me.PictureBox50.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox50.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox50.TabIndex = 56
        Me.PictureBox50.TabStop = False
        '
        'PictureBox51
        '
        Me.PictureBox51.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox51.Location = New System.Drawing.Point(231, 61)
        Me.PictureBox51.Name = "PictureBox51"
        Me.PictureBox51.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox51.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox51.TabIndex = 55
        Me.PictureBox51.TabStop = False
        '
        'PictureBox52
        '
        Me.PictureBox52.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox52.Location = New System.Drawing.Point(174, 61)
        Me.PictureBox52.Name = "PictureBox52"
        Me.PictureBox52.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox52.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox52.TabIndex = 54
        Me.PictureBox52.TabStop = False
        '
        'PictureBox53
        '
        Me.PictureBox53.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox53.Location = New System.Drawing.Point(117, 61)
        Me.PictureBox53.Name = "PictureBox53"
        Me.PictureBox53.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox53.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox53.TabIndex = 53
        Me.PictureBox53.TabStop = False
        '
        'PictureBox54
        '
        Me.PictureBox54.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox54.Location = New System.Drawing.Point(60, 61)
        Me.PictureBox54.Name = "PictureBox54"
        Me.PictureBox54.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox54.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox54.TabIndex = 52
        Me.PictureBox54.TabStop = False
        '
        'PictureBox33
        '
        Me.PictureBox33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox33.Location = New System.Drawing.Point(629, 351)
        Me.PictureBox33.Name = "PictureBox33"
        Me.PictureBox33.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox33.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox33.TabIndex = 51
        Me.PictureBox33.TabStop = False
        '
        'PictureBox34
        '
        Me.PictureBox34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox34.Location = New System.Drawing.Point(572, 351)
        Me.PictureBox34.Name = "PictureBox34"
        Me.PictureBox34.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox34.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox34.TabIndex = 50
        Me.PictureBox34.TabStop = False
        '
        'PictureBox35
        '
        Me.PictureBox35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox35.Location = New System.Drawing.Point(515, 351)
        Me.PictureBox35.Name = "PictureBox35"
        Me.PictureBox35.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox35.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox35.TabIndex = 49
        Me.PictureBox35.TabStop = False
        '
        'PictureBox36
        '
        Me.PictureBox36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox36.Location = New System.Drawing.Point(458, 351)
        Me.PictureBox36.Name = "PictureBox36"
        Me.PictureBox36.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox36.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox36.TabIndex = 48
        Me.PictureBox36.TabStop = False
        '
        'PictureBox37
        '
        Me.PictureBox37.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox37.Location = New System.Drawing.Point(401, 351)
        Me.PictureBox37.Name = "PictureBox37"
        Me.PictureBox37.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox37.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox37.TabIndex = 47
        Me.PictureBox37.TabStop = False
        '
        'PictureBox38
        '
        Me.PictureBox38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox38.Location = New System.Drawing.Point(344, 351)
        Me.PictureBox38.Name = "PictureBox38"
        Me.PictureBox38.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox38.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox38.TabIndex = 46
        Me.PictureBox38.TabStop = False
        '
        'PictureBox39
        '
        Me.PictureBox39.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox39.Location = New System.Drawing.Point(287, 351)
        Me.PictureBox39.Name = "PictureBox39"
        Me.PictureBox39.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox39.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox39.TabIndex = 45
        Me.PictureBox39.TabStop = False
        '
        'PictureBox40
        '
        Me.PictureBox40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox40.Location = New System.Drawing.Point(230, 351)
        Me.PictureBox40.Name = "PictureBox40"
        Me.PictureBox40.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox40.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox40.TabIndex = 44
        Me.PictureBox40.TabStop = False
        '
        'PictureBox41
        '
        Me.PictureBox41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox41.Location = New System.Drawing.Point(173, 351)
        Me.PictureBox41.Name = "PictureBox41"
        Me.PictureBox41.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox41.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox41.TabIndex = 43
        Me.PictureBox41.TabStop = False
        '
        'PictureBox42
        '
        Me.PictureBox42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox42.Location = New System.Drawing.Point(116, 351)
        Me.PictureBox42.Name = "PictureBox42"
        Me.PictureBox42.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox42.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox42.TabIndex = 42
        Me.PictureBox42.TabStop = False
        '
        'PictureBox43
        '
        Me.PictureBox43.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox43.Location = New System.Drawing.Point(59, 351)
        Me.PictureBox43.Name = "PictureBox43"
        Me.PictureBox43.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox43.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox43.TabIndex = 41
        Me.PictureBox43.TabStop = False
        '
        'PictureBox27
        '
        Me.PictureBox27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox27.Location = New System.Drawing.Point(687, 351)
        Me.PictureBox27.Name = "PictureBox27"
        Me.PictureBox27.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox27.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox27.TabIndex = 40
        Me.PictureBox27.TabStop = False
        '
        'PictureBox28
        '
        Me.PictureBox28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox28.Location = New System.Drawing.Point(687, 293)
        Me.PictureBox28.Name = "PictureBox28"
        Me.PictureBox28.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox28.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox28.TabIndex = 39
        Me.PictureBox28.TabStop = False
        '
        'PictureBox29
        '
        Me.PictureBox29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox29.Location = New System.Drawing.Point(687, 235)
        Me.PictureBox29.Name = "PictureBox29"
        Me.PictureBox29.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox29.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox29.TabIndex = 38
        Me.PictureBox29.TabStop = False
        '
        'PictureBox30
        '
        Me.PictureBox30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox30.Location = New System.Drawing.Point(687, 177)
        Me.PictureBox30.Name = "PictureBox30"
        Me.PictureBox30.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox30.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox30.TabIndex = 37
        Me.PictureBox30.TabStop = False
        '
        'PictureBox31
        '
        Me.PictureBox31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox31.Location = New System.Drawing.Point(687, 119)
        Me.PictureBox31.Name = "PictureBox31"
        Me.PictureBox31.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox31.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox31.TabIndex = 36
        Me.PictureBox31.TabStop = False
        '
        'PictureBox32
        '
        Me.PictureBox32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox32.Location = New System.Drawing.Point(687, 61)
        Me.PictureBox32.Name = "PictureBox32"
        Me.PictureBox32.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox32.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox32.TabIndex = 35
        Me.PictureBox32.TabStop = False
        '
        'PictureBox21
        '
        Me.PictureBox21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox21.Location = New System.Drawing.Point(687, 3)
        Me.PictureBox21.Name = "PictureBox21"
        Me.PictureBox21.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox21.TabIndex = 34
        Me.PictureBox21.TabStop = False
        '
        'PictureBox22
        '
        Me.PictureBox22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox22.Location = New System.Drawing.Point(630, 3)
        Me.PictureBox22.Name = "PictureBox22"
        Me.PictureBox22.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox22.TabIndex = 33
        Me.PictureBox22.TabStop = False
        '
        'PictureBox23
        '
        Me.PictureBox23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox23.Location = New System.Drawing.Point(573, 3)
        Me.PictureBox23.Name = "PictureBox23"
        Me.PictureBox23.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox23.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox23.TabIndex = 32
        Me.PictureBox23.TabStop = False
        '
        'PictureBox24
        '
        Me.PictureBox24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox24.Location = New System.Drawing.Point(516, 3)
        Me.PictureBox24.Name = "PictureBox24"
        Me.PictureBox24.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox24.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox24.TabIndex = 31
        Me.PictureBox24.TabStop = False
        '
        'PictureBox25
        '
        Me.PictureBox25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox25.Location = New System.Drawing.Point(459, 3)
        Me.PictureBox25.Name = "PictureBox25"
        Me.PictureBox25.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox25.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox25.TabIndex = 30
        Me.PictureBox25.TabStop = False
        '
        'PictureBox26
        '
        Me.PictureBox26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox26.Location = New System.Drawing.Point(402, 3)
        Me.PictureBox26.Name = "PictureBox26"
        Me.PictureBox26.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox26.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox26.TabIndex = 29
        Me.PictureBox26.TabStop = False
        '
        'PictureBox20
        '
        Me.PictureBox20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox20.Location = New System.Drawing.Point(345, 3)
        Me.PictureBox20.Name = "PictureBox20"
        Me.PictureBox20.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox20.TabIndex = 28
        Me.PictureBox20.TabStop = False
        '
        'PictureBox19
        '
        Me.PictureBox19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox19.Location = New System.Drawing.Point(288, 3)
        Me.PictureBox19.Name = "PictureBox19"
        Me.PictureBox19.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox19.TabIndex = 27
        Me.PictureBox19.TabStop = False
        '
        'PictureBox18
        '
        Me.PictureBox18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox18.Location = New System.Drawing.Point(231, 3)
        Me.PictureBox18.Name = "PictureBox18"
        Me.PictureBox18.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox18.TabIndex = 26
        Me.PictureBox18.TabStop = False
        '
        'PictureBox17
        '
        Me.PictureBox17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox17.Location = New System.Drawing.Point(174, 3)
        Me.PictureBox17.Name = "PictureBox17"
        Me.PictureBox17.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox17.TabIndex = 25
        Me.PictureBox17.TabStop = False
        '
        'PictureBox16
        '
        Me.PictureBox16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox16.Location = New System.Drawing.Point(117, 3)
        Me.PictureBox16.Name = "PictureBox16"
        Me.PictureBox16.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox16.TabIndex = 24
        Me.PictureBox16.TabStop = False
        '
        'PictureBox15
        '
        Me.PictureBox15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox15.Location = New System.Drawing.Point(60, 3)
        Me.PictureBox15.Name = "PictureBox15"
        Me.PictureBox15.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox15.TabIndex = 23
        Me.PictureBox15.TabStop = False
        '
        'PictureBox14
        '
        Me.PictureBox14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox14.Location = New System.Drawing.Point(3, 351)
        Me.PictureBox14.Name = "PictureBox14"
        Me.PictureBox14.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox14.TabIndex = 22
        Me.PictureBox14.TabStop = False
        '
        'PictureBox13
        '
        Me.PictureBox13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox13.Location = New System.Drawing.Point(3, 293)
        Me.PictureBox13.Name = "PictureBox13"
        Me.PictureBox13.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox13.TabIndex = 21
        Me.PictureBox13.TabStop = False
        '
        'PictureBox12
        '
        Me.PictureBox12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox12.Location = New System.Drawing.Point(3, 235)
        Me.PictureBox12.Name = "PictureBox12"
        Me.PictureBox12.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox12.TabIndex = 20
        Me.PictureBox12.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox2.Location = New System.Drawing.Point(3, 177)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox2.TabIndex = 19
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox1.Location = New System.Drawing.Point(3, 119)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox1.TabIndex = 18
        Me.PictureBox1.TabStop = False
        '
        'PictureBox11
        '
        Me.PictureBox11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox11.Location = New System.Drawing.Point(3, 61)
        Me.PictureBox11.Name = "PictureBox11"
        Me.PictureBox11.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox11.TabIndex = 17
        Me.PictureBox11.TabStop = False
        '
        'PictureBox10
        '
        Me.PictureBox10.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox10.Location = New System.Drawing.Point(3, 3)
        Me.PictureBox10.Name = "PictureBox10"
        Me.PictureBox10.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox10.TabIndex = 16
        Me.PictureBox10.TabStop = False
        '
        'Button1
        '
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.Location = New System.Drawing.Point(615, 734)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 29)
        Me.Button1.TabIndex = 16
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Image = CType(resources.GetObject("Button2.Image"), System.Drawing.Image)
        Me.Button2.Location = New System.Drawing.Point(415, 734)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(78, 29)
        Me.Button2.TabIndex = 17
        Me.Button2.UseVisualStyleBackColor = True
        '
        'PictureBox4
        '
        Me.PictureBox4.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox4.Location = New System.Drawing.Point(19, 722)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox4.TabIndex = 114
        Me.PictureBox4.TabStop = False
        '
        'PictureBox5
        '
        Me.PictureBox5.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox5.Location = New System.Drawing.Point(76, 722)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox5.TabIndex = 115
        Me.PictureBox5.TabStop = False
        '
        'PictureBox6
        '
        Me.PictureBox6.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox6.Location = New System.Drawing.Point(133, 722)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox6.TabIndex = 116
        Me.PictureBox6.TabStop = False
        '
        'PictureBox7
        '
        Me.PictureBox7.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox7.Location = New System.Drawing.Point(190, 722)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox7.TabIndex = 117
        Me.PictureBox7.TabStop = False
        '
        'PictureBox8
        '
        Me.PictureBox8.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox8.Location = New System.Drawing.Point(247, 722)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox8.TabIndex = 118
        Me.PictureBox8.TabStop = False
        '
        'PictureBox9
        '
        Me.PictureBox9.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox9.Location = New System.Drawing.Point(304, 722)
        Me.PictureBox9.Name = "PictureBox9"
        Me.PictureBox9.Size = New System.Drawing.Size(51, 52)
        Me.PictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox9.TabIndex = 119
        Me.PictureBox9.TabStop = False
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 10
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 53.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 53.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 53.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 53.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 53.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 53.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 53.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 53.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 53.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 327.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox186, 0, 8)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox185, 9, 7)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox184, 8, 7)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox183, 7, 7)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox182, 6, 7)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox181, 5, 7)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox180, 4, 7)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox179, 3, 7)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox178, 2, 7)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox177, 1, 7)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox176, 0, 7)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox175, 9, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox174, 8, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox173, 7, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox172, 6, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox171, 5, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox170, 4, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox169, 3, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox168, 2, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox167, 1, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox166, 0, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox165, 9, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox164, 8, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox163, 7, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox162, 6, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox161, 5, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox160, 4, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox159, 3, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox158, 2, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox157, 1, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox156, 0, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox155, 9, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox154, 8, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox153, 7, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox152, 6, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox151, 5, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox150, 4, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox149, 3, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox148, 2, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox147, 1, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox146, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox145, 9, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox144, 8, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox143, 7, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox142, 6, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox141, 5, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox140, 4, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox139, 3, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox138, 2, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox137, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox136, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox135, 9, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox134, 8, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox133, 7, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox132, 6, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox131, 5, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox130, 4, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox129, 3, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox128, 2, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox127, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox126, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox125, 9, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox124, 8, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox123, 7, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox122, 6, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox121, 5, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox120, 4, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox119, 3, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox118, 2, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox117, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox116, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox115, 9, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox114, 8, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox113, 7, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox112, 6, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox111, 5, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox110, 4, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox109, 3, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox108, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox107, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox106, 0, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(31, 75)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 10
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(804, 515)
        Me.TableLayoutPanel1.TabIndex = 114
        '
        'PictureBox106
        '
        Me.PictureBox106.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox106.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox106.Location = New System.Drawing.Point(3, 3)
        Me.PictureBox106.Name = "PictureBox106"
        Me.PictureBox106.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox106.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox106.TabIndex = 120
        Me.PictureBox106.TabStop = False
        '
        'PictureBox107
        '
        Me.PictureBox107.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox107.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox107.Location = New System.Drawing.Point(56, 3)
        Me.PictureBox107.Name = "PictureBox107"
        Me.PictureBox107.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox107.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox107.TabIndex = 121
        Me.PictureBox107.TabStop = False
        '
        'PictureBox108
        '
        Me.PictureBox108.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox108.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox108.Location = New System.Drawing.Point(109, 3)
        Me.PictureBox108.Name = "PictureBox108"
        Me.PictureBox108.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox108.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox108.TabIndex = 122
        Me.PictureBox108.TabStop = False
        '
        'PictureBox109
        '
        Me.PictureBox109.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox109.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox109.Location = New System.Drawing.Point(162, 3)
        Me.PictureBox109.Name = "PictureBox109"
        Me.PictureBox109.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox109.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox109.TabIndex = 123
        Me.PictureBox109.TabStop = False
        '
        'PictureBox110
        '
        Me.PictureBox110.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox110.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox110.Location = New System.Drawing.Point(215, 3)
        Me.PictureBox110.Name = "PictureBox110"
        Me.PictureBox110.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox110.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox110.TabIndex = 124
        Me.PictureBox110.TabStop = False
        '
        'PictureBox111
        '
        Me.PictureBox111.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox111.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox111.Location = New System.Drawing.Point(268, 3)
        Me.PictureBox111.Name = "PictureBox111"
        Me.PictureBox111.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox111.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox111.TabIndex = 125
        Me.PictureBox111.TabStop = False
        '
        'PictureBox112
        '
        Me.PictureBox112.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox112.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox112.Location = New System.Drawing.Point(321, 3)
        Me.PictureBox112.Name = "PictureBox112"
        Me.PictureBox112.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox112.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox112.TabIndex = 126
        Me.PictureBox112.TabStop = False
        '
        'PictureBox113
        '
        Me.PictureBox113.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox113.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox113.Location = New System.Drawing.Point(374, 3)
        Me.PictureBox113.Name = "PictureBox113"
        Me.PictureBox113.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox113.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox113.TabIndex = 127
        Me.PictureBox113.TabStop = False
        '
        'PictureBox114
        '
        Me.PictureBox114.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox114.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox114.Location = New System.Drawing.Point(427, 3)
        Me.PictureBox114.Name = "PictureBox114"
        Me.PictureBox114.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox114.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox114.TabIndex = 128
        Me.PictureBox114.TabStop = False
        '
        'PictureBox115
        '
        Me.PictureBox115.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox115.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox115.Location = New System.Drawing.Point(480, 3)
        Me.PictureBox115.Name = "PictureBox115"
        Me.PictureBox115.Size = New System.Drawing.Size(51, 48)
        Me.PictureBox115.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox115.TabIndex = 129
        Me.PictureBox115.TabStop = False
        '
        'PictureBox116
        '
        Me.PictureBox116.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox116.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox116.Location = New System.Drawing.Point(3, 57)
        Me.PictureBox116.Name = "PictureBox116"
        Me.PictureBox116.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox116.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox116.TabIndex = 130
        Me.PictureBox116.TabStop = False
        '
        'PictureBox117
        '
        Me.PictureBox117.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox117.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox117.Location = New System.Drawing.Point(56, 57)
        Me.PictureBox117.Name = "PictureBox117"
        Me.PictureBox117.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox117.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox117.TabIndex = 131
        Me.PictureBox117.TabStop = False
        '
        'PictureBox118
        '
        Me.PictureBox118.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox118.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox118.Location = New System.Drawing.Point(109, 57)
        Me.PictureBox118.Name = "PictureBox118"
        Me.PictureBox118.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox118.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox118.TabIndex = 132
        Me.PictureBox118.TabStop = False
        '
        'PictureBox119
        '
        Me.PictureBox119.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox119.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox119.Location = New System.Drawing.Point(162, 57)
        Me.PictureBox119.Name = "PictureBox119"
        Me.PictureBox119.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox119.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox119.TabIndex = 133
        Me.PictureBox119.TabStop = False
        '
        'PictureBox120
        '
        Me.PictureBox120.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox120.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox120.Location = New System.Drawing.Point(215, 57)
        Me.PictureBox120.Name = "PictureBox120"
        Me.PictureBox120.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox120.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox120.TabIndex = 134
        Me.PictureBox120.TabStop = False
        '
        'PictureBox121
        '
        Me.PictureBox121.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox121.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox121.Location = New System.Drawing.Point(268, 57)
        Me.PictureBox121.Name = "PictureBox121"
        Me.PictureBox121.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox121.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox121.TabIndex = 135
        Me.PictureBox121.TabStop = False
        '
        'PictureBox122
        '
        Me.PictureBox122.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox122.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox122.Location = New System.Drawing.Point(321, 57)
        Me.PictureBox122.Name = "PictureBox122"
        Me.PictureBox122.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox122.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox122.TabIndex = 136
        Me.PictureBox122.TabStop = False
        '
        'PictureBox123
        '
        Me.PictureBox123.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox123.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox123.Location = New System.Drawing.Point(374, 57)
        Me.PictureBox123.Name = "PictureBox123"
        Me.PictureBox123.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox123.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox123.TabIndex = 137
        Me.PictureBox123.TabStop = False
        '
        'PictureBox124
        '
        Me.PictureBox124.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox124.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox124.Location = New System.Drawing.Point(427, 57)
        Me.PictureBox124.Name = "PictureBox124"
        Me.PictureBox124.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox124.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox124.TabIndex = 138
        Me.PictureBox124.TabStop = False
        '
        'PictureBox125
        '
        Me.PictureBox125.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox125.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox125.Location = New System.Drawing.Point(480, 57)
        Me.PictureBox125.Name = "PictureBox125"
        Me.PictureBox125.Size = New System.Drawing.Size(51, 48)
        Me.PictureBox125.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox125.TabIndex = 139
        Me.PictureBox125.TabStop = False
        '
        'PictureBox126
        '
        Me.PictureBox126.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox126.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox126.Location = New System.Drawing.Point(3, 111)
        Me.PictureBox126.Name = "PictureBox126"
        Me.PictureBox126.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox126.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox126.TabIndex = 140
        Me.PictureBox126.TabStop = False
        '
        'PictureBox127
        '
        Me.PictureBox127.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox127.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox127.Location = New System.Drawing.Point(56, 111)
        Me.PictureBox127.Name = "PictureBox127"
        Me.PictureBox127.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox127.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox127.TabIndex = 141
        Me.PictureBox127.TabStop = False
        '
        'PictureBox128
        '
        Me.PictureBox128.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox128.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox128.Location = New System.Drawing.Point(109, 111)
        Me.PictureBox128.Name = "PictureBox128"
        Me.PictureBox128.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox128.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox128.TabIndex = 142
        Me.PictureBox128.TabStop = False
        '
        'PictureBox129
        '
        Me.PictureBox129.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox129.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox129.Location = New System.Drawing.Point(162, 111)
        Me.PictureBox129.Name = "PictureBox129"
        Me.PictureBox129.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox129.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox129.TabIndex = 143
        Me.PictureBox129.TabStop = False
        '
        'PictureBox130
        '
        Me.PictureBox130.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox130.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox130.Location = New System.Drawing.Point(215, 111)
        Me.PictureBox130.Name = "PictureBox130"
        Me.PictureBox130.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox130.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox130.TabIndex = 144
        Me.PictureBox130.TabStop = False
        '
        'PictureBox131
        '
        Me.PictureBox131.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox131.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox131.Location = New System.Drawing.Point(268, 111)
        Me.PictureBox131.Name = "PictureBox131"
        Me.PictureBox131.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox131.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox131.TabIndex = 145
        Me.PictureBox131.TabStop = False
        '
        'PictureBox132
        '
        Me.PictureBox132.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox132.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox132.Location = New System.Drawing.Point(321, 111)
        Me.PictureBox132.Name = "PictureBox132"
        Me.PictureBox132.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox132.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox132.TabIndex = 146
        Me.PictureBox132.TabStop = False
        '
        'PictureBox133
        '
        Me.PictureBox133.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox133.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox133.Location = New System.Drawing.Point(374, 111)
        Me.PictureBox133.Name = "PictureBox133"
        Me.PictureBox133.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox133.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox133.TabIndex = 147
        Me.PictureBox133.TabStop = False
        '
        'PictureBox134
        '
        Me.PictureBox134.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox134.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox134.Location = New System.Drawing.Point(427, 111)
        Me.PictureBox134.Name = "PictureBox134"
        Me.PictureBox134.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox134.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox134.TabIndex = 148
        Me.PictureBox134.TabStop = False
        '
        'PictureBox135
        '
        Me.PictureBox135.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox135.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox135.Location = New System.Drawing.Point(480, 111)
        Me.PictureBox135.Name = "PictureBox135"
        Me.PictureBox135.Size = New System.Drawing.Size(51, 48)
        Me.PictureBox135.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox135.TabIndex = 149
        Me.PictureBox135.TabStop = False
        '
        'PictureBox136
        '
        Me.PictureBox136.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox136.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox136.Location = New System.Drawing.Point(3, 165)
        Me.PictureBox136.Name = "PictureBox136"
        Me.PictureBox136.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox136.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox136.TabIndex = 150
        Me.PictureBox136.TabStop = False
        '
        'PictureBox137
        '
        Me.PictureBox137.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox137.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox137.Location = New System.Drawing.Point(56, 165)
        Me.PictureBox137.Name = "PictureBox137"
        Me.PictureBox137.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox137.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox137.TabIndex = 151
        Me.PictureBox137.TabStop = False
        '
        'PictureBox138
        '
        Me.PictureBox138.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox138.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox138.Location = New System.Drawing.Point(109, 165)
        Me.PictureBox138.Name = "PictureBox138"
        Me.PictureBox138.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox138.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox138.TabIndex = 152
        Me.PictureBox138.TabStop = False
        '
        'PictureBox139
        '
        Me.PictureBox139.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox139.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox139.Location = New System.Drawing.Point(162, 165)
        Me.PictureBox139.Name = "PictureBox139"
        Me.PictureBox139.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox139.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox139.TabIndex = 153
        Me.PictureBox139.TabStop = False
        '
        'PictureBox140
        '
        Me.PictureBox140.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox140.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox140.Location = New System.Drawing.Point(215, 165)
        Me.PictureBox140.Name = "PictureBox140"
        Me.PictureBox140.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox140.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox140.TabIndex = 154
        Me.PictureBox140.TabStop = False
        '
        'PictureBox141
        '
        Me.PictureBox141.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox141.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox141.Location = New System.Drawing.Point(268, 165)
        Me.PictureBox141.Name = "PictureBox141"
        Me.PictureBox141.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox141.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox141.TabIndex = 155
        Me.PictureBox141.TabStop = False
        '
        'PictureBox142
        '
        Me.PictureBox142.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox142.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox142.Location = New System.Drawing.Point(321, 165)
        Me.PictureBox142.Name = "PictureBox142"
        Me.PictureBox142.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox142.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox142.TabIndex = 156
        Me.PictureBox142.TabStop = False
        '
        'PictureBox143
        '
        Me.PictureBox143.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox143.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox143.Location = New System.Drawing.Point(374, 165)
        Me.PictureBox143.Name = "PictureBox143"
        Me.PictureBox143.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox143.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox143.TabIndex = 157
        Me.PictureBox143.TabStop = False
        '
        'PictureBox144
        '
        Me.PictureBox144.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox144.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox144.Location = New System.Drawing.Point(427, 165)
        Me.PictureBox144.Name = "PictureBox144"
        Me.PictureBox144.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox144.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox144.TabIndex = 158
        Me.PictureBox144.TabStop = False
        '
        'PictureBox145
        '
        Me.PictureBox145.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox145.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox145.Location = New System.Drawing.Point(480, 165)
        Me.PictureBox145.Name = "PictureBox145"
        Me.PictureBox145.Size = New System.Drawing.Size(51, 48)
        Me.PictureBox145.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox145.TabIndex = 159
        Me.PictureBox145.TabStop = False
        '
        'PictureBox146
        '
        Me.PictureBox146.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox146.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox146.Location = New System.Drawing.Point(3, 219)
        Me.PictureBox146.Name = "PictureBox146"
        Me.PictureBox146.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox146.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox146.TabIndex = 160
        Me.PictureBox146.TabStop = False
        '
        'PictureBox147
        '
        Me.PictureBox147.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox147.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox147.Location = New System.Drawing.Point(56, 219)
        Me.PictureBox147.Name = "PictureBox147"
        Me.PictureBox147.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox147.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox147.TabIndex = 161
        Me.PictureBox147.TabStop = False
        '
        'PictureBox148
        '
        Me.PictureBox148.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox148.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox148.Location = New System.Drawing.Point(109, 219)
        Me.PictureBox148.Name = "PictureBox148"
        Me.PictureBox148.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox148.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox148.TabIndex = 162
        Me.PictureBox148.TabStop = False
        '
        'PictureBox149
        '
        Me.PictureBox149.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox149.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox149.Location = New System.Drawing.Point(162, 219)
        Me.PictureBox149.Name = "PictureBox149"
        Me.PictureBox149.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox149.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox149.TabIndex = 163
        Me.PictureBox149.TabStop = False
        '
        'PictureBox150
        '
        Me.PictureBox150.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox150.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox150.Location = New System.Drawing.Point(215, 219)
        Me.PictureBox150.Name = "PictureBox150"
        Me.PictureBox150.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox150.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox150.TabIndex = 164
        Me.PictureBox150.TabStop = False
        '
        'PictureBox151
        '
        Me.PictureBox151.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox151.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox151.Location = New System.Drawing.Point(268, 219)
        Me.PictureBox151.Name = "PictureBox151"
        Me.PictureBox151.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox151.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox151.TabIndex = 165
        Me.PictureBox151.TabStop = False
        '
        'PictureBox152
        '
        Me.PictureBox152.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox152.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox152.Location = New System.Drawing.Point(321, 219)
        Me.PictureBox152.Name = "PictureBox152"
        Me.PictureBox152.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox152.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox152.TabIndex = 166
        Me.PictureBox152.TabStop = False
        '
        'PictureBox153
        '
        Me.PictureBox153.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox153.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox153.Location = New System.Drawing.Point(374, 219)
        Me.PictureBox153.Name = "PictureBox153"
        Me.PictureBox153.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox153.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox153.TabIndex = 167
        Me.PictureBox153.TabStop = False
        '
        'PictureBox154
        '
        Me.PictureBox154.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox154.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox154.Location = New System.Drawing.Point(427, 219)
        Me.PictureBox154.Name = "PictureBox154"
        Me.PictureBox154.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox154.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox154.TabIndex = 168
        Me.PictureBox154.TabStop = False
        '
        'PictureBox155
        '
        Me.PictureBox155.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox155.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox155.Location = New System.Drawing.Point(480, 219)
        Me.PictureBox155.Name = "PictureBox155"
        Me.PictureBox155.Size = New System.Drawing.Size(51, 48)
        Me.PictureBox155.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox155.TabIndex = 169
        Me.PictureBox155.TabStop = False
        '
        'PictureBox156
        '
        Me.PictureBox156.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox156.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox156.Location = New System.Drawing.Point(3, 273)
        Me.PictureBox156.Name = "PictureBox156"
        Me.PictureBox156.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox156.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox156.TabIndex = 170
        Me.PictureBox156.TabStop = False
        '
        'PictureBox157
        '
        Me.PictureBox157.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox157.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox157.Location = New System.Drawing.Point(56, 273)
        Me.PictureBox157.Name = "PictureBox157"
        Me.PictureBox157.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox157.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox157.TabIndex = 171
        Me.PictureBox157.TabStop = False
        '
        'PictureBox158
        '
        Me.PictureBox158.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox158.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox158.Location = New System.Drawing.Point(109, 273)
        Me.PictureBox158.Name = "PictureBox158"
        Me.PictureBox158.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox158.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox158.TabIndex = 172
        Me.PictureBox158.TabStop = False
        '
        'PictureBox159
        '
        Me.PictureBox159.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox159.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox159.Location = New System.Drawing.Point(162, 273)
        Me.PictureBox159.Name = "PictureBox159"
        Me.PictureBox159.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox159.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox159.TabIndex = 173
        Me.PictureBox159.TabStop = False
        '
        'PictureBox160
        '
        Me.PictureBox160.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox160.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox160.Location = New System.Drawing.Point(215, 273)
        Me.PictureBox160.Name = "PictureBox160"
        Me.PictureBox160.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox160.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox160.TabIndex = 174
        Me.PictureBox160.TabStop = False
        '
        'PictureBox161
        '
        Me.PictureBox161.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox161.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox161.Location = New System.Drawing.Point(268, 273)
        Me.PictureBox161.Name = "PictureBox161"
        Me.PictureBox161.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox161.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox161.TabIndex = 175
        Me.PictureBox161.TabStop = False
        '
        'PictureBox162
        '
        Me.PictureBox162.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox162.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox162.Location = New System.Drawing.Point(321, 273)
        Me.PictureBox162.Name = "PictureBox162"
        Me.PictureBox162.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox162.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox162.TabIndex = 176
        Me.PictureBox162.TabStop = False
        '
        'PictureBox163
        '
        Me.PictureBox163.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox163.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox163.Location = New System.Drawing.Point(374, 273)
        Me.PictureBox163.Name = "PictureBox163"
        Me.PictureBox163.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox163.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox163.TabIndex = 177
        Me.PictureBox163.TabStop = False
        '
        'PictureBox164
        '
        Me.PictureBox164.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox164.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox164.Location = New System.Drawing.Point(427, 273)
        Me.PictureBox164.Name = "PictureBox164"
        Me.PictureBox164.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox164.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox164.TabIndex = 178
        Me.PictureBox164.TabStop = False
        '
        'PictureBox165
        '
        Me.PictureBox165.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox165.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox165.Location = New System.Drawing.Point(480, 273)
        Me.PictureBox165.Name = "PictureBox165"
        Me.PictureBox165.Size = New System.Drawing.Size(51, 48)
        Me.PictureBox165.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox165.TabIndex = 179
        Me.PictureBox165.TabStop = False
        '
        'PictureBox166
        '
        Me.PictureBox166.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox166.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox166.Location = New System.Drawing.Point(3, 327)
        Me.PictureBox166.Name = "PictureBox166"
        Me.PictureBox166.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox166.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox166.TabIndex = 180
        Me.PictureBox166.TabStop = False
        '
        'PictureBox167
        '
        Me.PictureBox167.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox167.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox167.Location = New System.Drawing.Point(56, 327)
        Me.PictureBox167.Name = "PictureBox167"
        Me.PictureBox167.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox167.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox167.TabIndex = 181
        Me.PictureBox167.TabStop = False
        '
        'PictureBox168
        '
        Me.PictureBox168.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox168.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox168.Location = New System.Drawing.Point(109, 327)
        Me.PictureBox168.Name = "PictureBox168"
        Me.PictureBox168.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox168.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox168.TabIndex = 182
        Me.PictureBox168.TabStop = False
        '
        'PictureBox169
        '
        Me.PictureBox169.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox169.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox169.Location = New System.Drawing.Point(162, 327)
        Me.PictureBox169.Name = "PictureBox169"
        Me.PictureBox169.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox169.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox169.TabIndex = 183
        Me.PictureBox169.TabStop = False
        '
        'PictureBox170
        '
        Me.PictureBox170.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox170.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox170.Location = New System.Drawing.Point(215, 327)
        Me.PictureBox170.Name = "PictureBox170"
        Me.PictureBox170.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox170.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox170.TabIndex = 184
        Me.PictureBox170.TabStop = False
        '
        'PictureBox171
        '
        Me.PictureBox171.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox171.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox171.Location = New System.Drawing.Point(268, 327)
        Me.PictureBox171.Name = "PictureBox171"
        Me.PictureBox171.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox171.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox171.TabIndex = 185
        Me.PictureBox171.TabStop = False
        '
        'PictureBox172
        '
        Me.PictureBox172.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox172.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox172.Location = New System.Drawing.Point(321, 327)
        Me.PictureBox172.Name = "PictureBox172"
        Me.PictureBox172.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox172.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox172.TabIndex = 186
        Me.PictureBox172.TabStop = False
        '
        'PictureBox173
        '
        Me.PictureBox173.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox173.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox173.Location = New System.Drawing.Point(374, 327)
        Me.PictureBox173.Name = "PictureBox173"
        Me.PictureBox173.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox173.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox173.TabIndex = 187
        Me.PictureBox173.TabStop = False
        '
        'PictureBox174
        '
        Me.PictureBox174.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox174.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox174.Location = New System.Drawing.Point(427, 327)
        Me.PictureBox174.Name = "PictureBox174"
        Me.PictureBox174.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox174.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox174.TabIndex = 188
        Me.PictureBox174.TabStop = False
        '
        'PictureBox175
        '
        Me.PictureBox175.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox175.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox175.Location = New System.Drawing.Point(480, 327)
        Me.PictureBox175.Name = "PictureBox175"
        Me.PictureBox175.Size = New System.Drawing.Size(51, 48)
        Me.PictureBox175.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox175.TabIndex = 189
        Me.PictureBox175.TabStop = False
        '
        'PictureBox176
        '
        Me.PictureBox176.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox176.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox176.Location = New System.Drawing.Point(3, 381)
        Me.PictureBox176.Name = "PictureBox176"
        Me.PictureBox176.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox176.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox176.TabIndex = 190
        Me.PictureBox176.TabStop = False
        '
        'PictureBox177
        '
        Me.PictureBox177.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox177.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox177.Location = New System.Drawing.Point(56, 381)
        Me.PictureBox177.Name = "PictureBox177"
        Me.PictureBox177.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox177.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox177.TabIndex = 191
        Me.PictureBox177.TabStop = False
        '
        'PictureBox178
        '
        Me.PictureBox178.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox178.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox178.Location = New System.Drawing.Point(109, 381)
        Me.PictureBox178.Name = "PictureBox178"
        Me.PictureBox178.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox178.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox178.TabIndex = 192
        Me.PictureBox178.TabStop = False
        '
        'PictureBox179
        '
        Me.PictureBox179.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox179.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox179.Location = New System.Drawing.Point(162, 381)
        Me.PictureBox179.Name = "PictureBox179"
        Me.PictureBox179.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox179.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox179.TabIndex = 193
        Me.PictureBox179.TabStop = False
        '
        'PictureBox180
        '
        Me.PictureBox180.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox180.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox180.Location = New System.Drawing.Point(215, 381)
        Me.PictureBox180.Name = "PictureBox180"
        Me.PictureBox180.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox180.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox180.TabIndex = 194
        Me.PictureBox180.TabStop = False
        '
        'PictureBox181
        '
        Me.PictureBox181.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox181.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox181.Location = New System.Drawing.Point(268, 381)
        Me.PictureBox181.Name = "PictureBox181"
        Me.PictureBox181.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox181.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox181.TabIndex = 195
        Me.PictureBox181.TabStop = False
        '
        'PictureBox182
        '
        Me.PictureBox182.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox182.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox182.Location = New System.Drawing.Point(321, 381)
        Me.PictureBox182.Name = "PictureBox182"
        Me.PictureBox182.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox182.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox182.TabIndex = 196
        Me.PictureBox182.TabStop = False
        '
        'PictureBox183
        '
        Me.PictureBox183.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox183.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox183.Location = New System.Drawing.Point(374, 381)
        Me.PictureBox183.Name = "PictureBox183"
        Me.PictureBox183.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox183.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox183.TabIndex = 197
        Me.PictureBox183.TabStop = False
        '
        'PictureBox184
        '
        Me.PictureBox184.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox184.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox184.Location = New System.Drawing.Point(427, 381)
        Me.PictureBox184.Name = "PictureBox184"
        Me.PictureBox184.Size = New System.Drawing.Size(47, 48)
        Me.PictureBox184.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox184.TabIndex = 198
        Me.PictureBox184.TabStop = False
        '
        'PictureBox185
        '
        Me.PictureBox185.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox185.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox185.Location = New System.Drawing.Point(480, 381)
        Me.PictureBox185.Name = "PictureBox185"
        Me.PictureBox185.Size = New System.Drawing.Size(51, 48)
        Me.PictureBox185.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox185.TabIndex = 199
        Me.PictureBox185.TabStop = False
        '
        'PictureBox186
        '
        Me.PictureBox186.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox186.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox186.Location = New System.Drawing.Point(377, 233)
        Me.PictureBox186.Name = "PictureBox186"
        Me.PictureBox186.Size = New System.Drawing.Size(51, 48)
        Me.PictureBox186.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox186.TabIndex = 200
        Me.PictureBox186.TabStop = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1026, 882)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.PictureBox9)
        Me.Controls.Add(Me.PictureBox8)
        Me.Controls.Add(Me.PictureBox7)
        Me.Controls.Add(Me.PictureBox6)
        Me.Controls.Add(Me.PictureBox5)
        Me.Controls.Add(Me.PictureBox4)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.btnFindePartie)
        Me.Controls.Add(Me.labelTuilesRestantes)
        Me.Controls.Add(Me.labelNombreTuilesRest)
        Me.Controls.Add(Me.grpboxJoueurs)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnMenu)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.grpboxJoueurs.ResumeLayout(False)
        Me.grpboxJoueurs.PerformLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox100, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox101, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox102, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox103, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox104, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox105, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox99, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox88, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox89, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox90, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox91, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox92, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox93, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox94, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox95, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox96, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox97, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox98, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox77, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox78, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox79, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox80, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox81, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox82, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox83, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox84, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox85, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox86, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox87, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox66, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox67, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox68, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox69, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox70, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox71, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox72, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox73, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox74, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox75, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox76, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox55, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox56, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox57, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox58, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox59, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox60, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox61, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox62, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox63, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox64, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox65, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox44, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox45, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox46, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox47, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox48, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox49, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox50, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox51, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox52, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox53, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox54, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox38, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox39, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox40, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.PictureBox106, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox107, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox108, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox109, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox110, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox111, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox112, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox113, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox114, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox115, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox116, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox117, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox118, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox119, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox120, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox121, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox122, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox123, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox124, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox125, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox126, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox127, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox128, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox129, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox130, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox131, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox132, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox133, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox134, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox135, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox136, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox137, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox138, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox139, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox140, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox141, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox142, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox143, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox144, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox145, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox146, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox147, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox148, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox149, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox150, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox151, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox152, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox153, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox154, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox155, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox156, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox157, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox158, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox159, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox160, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox161, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox162, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox163, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox164, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox165, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox166, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox167, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox168, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox169, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox170, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox171, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox172, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox173, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox174, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox175, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox176, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox177, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox178, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox179, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox180, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox181, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox182, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox183, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox184, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox185, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox186, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnMenu As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents grpboxJoueurs As GroupBox
    Friend WithEvents labelScore4 As Label
    Friend WithEvents labelScore3 As Label
    Friend WithEvents labelScore2 As Label
    Friend WithEvents labelScore1 As Label
    Friend WithEvents labelJoueur4 As Label
    Friend WithEvents labelJoueur3 As Label
    Friend WithEvents labelJoueur2 As Label
    Friend WithEvents labelJoueur1 As Label
    Friend WithEvents labelNombreTuilesRest As Label
    Friend WithEvents labelTuilesRestantes As Label
    Friend WithEvents btnFindePartie As Button
    Friend WithEvents PictureBox3 As PictureBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents PictureBox11 As PictureBox
    Friend WithEvents PictureBox10 As PictureBox
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents PictureBox100 As PictureBox
    Friend WithEvents PictureBox101 As PictureBox
    Friend WithEvents PictureBox102 As PictureBox
    Friend WithEvents PictureBox103 As PictureBox
    Friend WithEvents PictureBox104 As PictureBox
    Friend WithEvents PictureBox105 As PictureBox
    Friend WithEvents PictureBox99 As PictureBox
    Friend WithEvents PictureBox88 As PictureBox
    Friend WithEvents PictureBox89 As PictureBox
    Friend WithEvents PictureBox90 As PictureBox
    Friend WithEvents PictureBox91 As PictureBox
    Friend WithEvents PictureBox92 As PictureBox
    Friend WithEvents PictureBox93 As PictureBox
    Friend WithEvents PictureBox94 As PictureBox
    Friend WithEvents PictureBox95 As PictureBox
    Friend WithEvents PictureBox96 As PictureBox
    Friend WithEvents PictureBox97 As PictureBox
    Friend WithEvents PictureBox98 As PictureBox
    Friend WithEvents PictureBox77 As PictureBox
    Friend WithEvents PictureBox78 As PictureBox
    Friend WithEvents PictureBox79 As PictureBox
    Friend WithEvents PictureBox80 As PictureBox
    Friend WithEvents PictureBox81 As PictureBox
    Friend WithEvents PictureBox82 As PictureBox
    Friend WithEvents PictureBox83 As PictureBox
    Friend WithEvents PictureBox84 As PictureBox
    Friend WithEvents PictureBox85 As PictureBox
    Friend WithEvents PictureBox86 As PictureBox
    Friend WithEvents PictureBox87 As PictureBox
    Friend WithEvents PictureBox66 As PictureBox
    Friend WithEvents PictureBox67 As PictureBox
    Friend WithEvents PictureBox68 As PictureBox
    Friend WithEvents PictureBox69 As PictureBox
    Friend WithEvents PictureBox70 As PictureBox
    Friend WithEvents PictureBox71 As PictureBox
    Friend WithEvents PictureBox72 As PictureBox
    Friend WithEvents PictureBox73 As PictureBox
    Friend WithEvents PictureBox74 As PictureBox
    Friend WithEvents PictureBox75 As PictureBox
    Friend WithEvents PictureBox76 As PictureBox
    Friend WithEvents PictureBox55 As PictureBox
    Friend WithEvents PictureBox56 As PictureBox
    Friend WithEvents PictureBox57 As PictureBox
    Friend WithEvents PictureBox58 As PictureBox
    Friend WithEvents PictureBox59 As PictureBox
    Friend WithEvents PictureBox60 As PictureBox
    Friend WithEvents PictureBox61 As PictureBox
    Friend WithEvents PictureBox62 As PictureBox
    Friend WithEvents PictureBox63 As PictureBox
    Friend WithEvents PictureBox64 As PictureBox
    Friend WithEvents PictureBox65 As PictureBox
    Friend WithEvents PictureBox44 As PictureBox
    Friend WithEvents PictureBox45 As PictureBox
    Friend WithEvents PictureBox46 As PictureBox
    Friend WithEvents PictureBox47 As PictureBox
    Friend WithEvents PictureBox48 As PictureBox
    Friend WithEvents PictureBox49 As PictureBox
    Friend WithEvents PictureBox50 As PictureBox
    Friend WithEvents PictureBox51 As PictureBox
    Friend WithEvents PictureBox52 As PictureBox
    Friend WithEvents PictureBox53 As PictureBox
    Friend WithEvents PictureBox54 As PictureBox
    Friend WithEvents PictureBox33 As PictureBox
    Friend WithEvents PictureBox34 As PictureBox
    Friend WithEvents PictureBox35 As PictureBox
    Friend WithEvents PictureBox36 As PictureBox
    Friend WithEvents PictureBox37 As PictureBox
    Friend WithEvents PictureBox38 As PictureBox
    Friend WithEvents PictureBox39 As PictureBox
    Friend WithEvents PictureBox40 As PictureBox
    Friend WithEvents PictureBox41 As PictureBox
    Friend WithEvents PictureBox42 As PictureBox
    Friend WithEvents PictureBox43 As PictureBox
    Friend WithEvents PictureBox27 As PictureBox
    Friend WithEvents PictureBox28 As PictureBox
    Friend WithEvents PictureBox29 As PictureBox
    Friend WithEvents PictureBox30 As PictureBox
    Friend WithEvents PictureBox31 As PictureBox
    Friend WithEvents PictureBox32 As PictureBox
    Friend WithEvents PictureBox21 As PictureBox
    Friend WithEvents PictureBox22 As PictureBox
    Friend WithEvents PictureBox23 As PictureBox
    Friend WithEvents PictureBox24 As PictureBox
    Friend WithEvents PictureBox25 As PictureBox
    Friend WithEvents PictureBox26 As PictureBox
    Friend WithEvents PictureBox20 As PictureBox
    Friend WithEvents PictureBox19 As PictureBox
    Friend WithEvents PictureBox18 As PictureBox
    Friend WithEvents PictureBox17 As PictureBox
    Friend WithEvents PictureBox16 As PictureBox
    Friend WithEvents PictureBox15 As PictureBox
    Friend WithEvents PictureBox14 As PictureBox
    Friend WithEvents PictureBox13 As PictureBox
    Friend WithEvents PictureBox12 As PictureBox
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents PictureBox4 As PictureBox
    Friend WithEvents PictureBox5 As PictureBox
    Friend WithEvents PictureBox6 As PictureBox
    Friend WithEvents PictureBox7 As PictureBox
    Friend WithEvents PictureBox8 As PictureBox
    Friend WithEvents PictureBox9 As PictureBox
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents PictureBox186 As PictureBox
    Friend WithEvents PictureBox185 As PictureBox
    Friend WithEvents PictureBox184 As PictureBox
    Friend WithEvents PictureBox183 As PictureBox
    Friend WithEvents PictureBox182 As PictureBox
    Friend WithEvents PictureBox181 As PictureBox
    Friend WithEvents PictureBox180 As PictureBox
    Friend WithEvents PictureBox179 As PictureBox
    Friend WithEvents PictureBox178 As PictureBox
    Friend WithEvents PictureBox177 As PictureBox
    Friend WithEvents PictureBox176 As PictureBox
    Friend WithEvents PictureBox175 As PictureBox
    Friend WithEvents PictureBox174 As PictureBox
    Friend WithEvents PictureBox173 As PictureBox
    Friend WithEvents PictureBox172 As PictureBox
    Friend WithEvents PictureBox171 As PictureBox
    Friend WithEvents PictureBox170 As PictureBox
    Friend WithEvents PictureBox169 As PictureBox
    Friend WithEvents PictureBox168 As PictureBox
    Friend WithEvents PictureBox167 As PictureBox
    Friend WithEvents PictureBox166 As PictureBox
    Friend WithEvents PictureBox165 As PictureBox
    Friend WithEvents PictureBox164 As PictureBox
    Friend WithEvents PictureBox163 As PictureBox
    Friend WithEvents PictureBox162 As PictureBox
    Friend WithEvents PictureBox161 As PictureBox
    Friend WithEvents PictureBox160 As PictureBox
    Friend WithEvents PictureBox159 As PictureBox
    Friend WithEvents PictureBox158 As PictureBox
    Friend WithEvents PictureBox157 As PictureBox
    Friend WithEvents PictureBox156 As PictureBox
    Friend WithEvents PictureBox155 As PictureBox
    Friend WithEvents PictureBox154 As PictureBox
    Friend WithEvents PictureBox153 As PictureBox
    Friend WithEvents PictureBox152 As PictureBox
    Friend WithEvents PictureBox151 As PictureBox
    Friend WithEvents PictureBox150 As PictureBox
    Friend WithEvents PictureBox149 As PictureBox
    Friend WithEvents PictureBox148 As PictureBox
    Friend WithEvents PictureBox147 As PictureBox
    Friend WithEvents PictureBox146 As PictureBox
    Friend WithEvents PictureBox145 As PictureBox
    Friend WithEvents PictureBox144 As PictureBox
    Friend WithEvents PictureBox143 As PictureBox
    Friend WithEvents PictureBox142 As PictureBox
    Friend WithEvents PictureBox141 As PictureBox
    Friend WithEvents PictureBox140 As PictureBox
    Friend WithEvents PictureBox139 As PictureBox
    Friend WithEvents PictureBox138 As PictureBox
    Friend WithEvents PictureBox137 As PictureBox
    Friend WithEvents PictureBox136 As PictureBox
    Friend WithEvents PictureBox135 As PictureBox
    Friend WithEvents PictureBox134 As PictureBox
    Friend WithEvents PictureBox133 As PictureBox
    Friend WithEvents PictureBox132 As PictureBox
    Friend WithEvents PictureBox131 As PictureBox
    Friend WithEvents PictureBox130 As PictureBox
    Friend WithEvents PictureBox129 As PictureBox
    Friend WithEvents PictureBox128 As PictureBox
    Friend WithEvents PictureBox127 As PictureBox
    Friend WithEvents PictureBox126 As PictureBox
    Friend WithEvents PictureBox125 As PictureBox
    Friend WithEvents PictureBox124 As PictureBox
    Friend WithEvents PictureBox123 As PictureBox
    Friend WithEvents PictureBox122 As PictureBox
    Friend WithEvents PictureBox121 As PictureBox
    Friend WithEvents PictureBox120 As PictureBox
    Friend WithEvents PictureBox119 As PictureBox
    Friend WithEvents PictureBox118 As PictureBox
    Friend WithEvents PictureBox117 As PictureBox
    Friend WithEvents PictureBox116 As PictureBox
    Friend WithEvents PictureBox115 As PictureBox
    Friend WithEvents PictureBox114 As PictureBox
    Friend WithEvents PictureBox113 As PictureBox
    Friend WithEvents PictureBox112 As PictureBox
    Friend WithEvents PictureBox111 As PictureBox
    Friend WithEvents PictureBox110 As PictureBox
    Friend WithEvents PictureBox109 As PictureBox
    Friend WithEvents PictureBox108 As PictureBox
    Friend WithEvents PictureBox107 As PictureBox
    Friend WithEvents PictureBox106 As PictureBox
End Class
