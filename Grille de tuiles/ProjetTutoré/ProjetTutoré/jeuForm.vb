﻿Public Class jeuForm
    Dim pctTuile(30, 30) As PictureBox
    Dim tableauPctBox As PictureBox()
    Dim tableauPions As String() = {"CarreBleu", "CarreJaune", "CarreOrange", "CarreRouge", "CarreVert", "CarreViolet", "CroixBleue", "CroixJaune", "CroixOrange", "CroixRouge", "CroixVerte", "CroixViolet", "EtoileBleue", "EtoileJaune", "EtoileOrange", "EtoileRouge", "EtoileVerte", "EtoileViolet", "LosangeBleu", "LosangeJaune", "LosangeOrange", "LosangeRouge", "LosangeVert", "LosangeViolet", "RondBleu", "RondJaune", "RondOrange", "RondRouge", "RondVert", "RondViolet", "TrefleBleu", "TrefleJaune", "TrefleOrange", "TrefleRouge", "TrefleVert", "TrefleViolet"}
    Dim tableTuiles As New TableLayoutPanel()
    Dim p As Integer
    Dim o As Integer
    Dim row As Integer
    Dim colonne As Integer
    Dim oui As Integer
    Dim fermela As Boolean

    Private Sub jeuForm_Load(sender As Object, e As EventArgs) Handles Me.Load
        tableauPctBox = {Me.pctBoxPioche0, Me.pctBoxPioche1, Me.pctBoxPioche2, Me.pctBoxPioche3, Me.pctBoxPioche4, Me.pctBoxPioche5}

        tableTuiles.Location = New System.Drawing.Point(185, 156)
        tableTuiles.Name = "tableTuiles"
        tableTuiles.Size = New System.Drawing.Size(500, 510)
        tableTuiles.ColumnCount = 30
        tableTuiles.RowCount = 30

        For i = 0 To 29

            For k = 0 To 29
                pctTuile(k, i) = New PictureBox
                pctTuile(k, i).Name = ("pctTuile" & k.ToString & i.ToString)
                pctTuile(k, i).BorderStyle = BorderStyle.FixedSingle
                pctTuile(k, i).Size = New Size(50, 51)
                pctTuile(k, i).AllowDrop = True
                tableTuiles.ColumnStyles.Add(New ColumnStyle(SizeType.AutoSize))
                tableTuiles.RowStyles.Add(New RowStyle(SizeType.AutoSize))
                AddHandler pctTuile(k, i).DragEnter, AddressOf pctTuile_DragEnter
                AddHandler pctTuile(k, i).DragDrop, AddressOf pctTuile_DragDrop
                AddHandler pctTuile(k, i).MouseMove, AddressOf pctTuile_MouseMove
                tableTuiles.Controls.Add(pctTuile(k, i), k, i)
                pctTuile(k, i).Margin = New Padding(0, 0, 0, 0)
            Next

        Next

        Controls.Add(tableTuiles)

        For i = 0 To 5
            Randomize()
            Dim random = CInt(Math.Ceiling(Rnd() * 35))
            tableauPctBox(i).Image = My.Resources.ResourceManager.GetObject(tableauPions(random))
            tableauPctBox(i).AllowDrop = True
        Next

    End Sub

    Private Sub pctBoxPioche0_MouseMove(sender As Object, e As MouseEventArgs) Handles pctBoxPioche5.MouseMove, pctBoxPioche4.MouseMove, pctBoxPioche3.MouseMove, pctBoxPioche2.MouseMove, pctBoxPioche1.MouseMove, pctBoxPioche0.MouseMove
        If e.Button = MouseButtons.Left Then
            sender.DoDragDrop(sender.Image, DragDropEffects.Move)
            sender.Image = Nothing
        End If

    End Sub

    Private Sub pctTuile_DragEnter(sender As Object, e As DragEventArgs)
        If e.Data.GetDataPresent(DataFormats.Bitmap) Then
            e.Effect = DragDropEffects.Move
        Else
            e.Effect = DragDropEffects.None
        End If

    End Sub

    Private Sub pctTuile_DragDrop(sender As Object, e As DragEventArgs)
        Dim image As Bitmap = DirectCast(e.Data.GetData(DataFormats.Bitmap, True), Bitmap)
        sender.Image = image
        Label1.Text = sender.Name
        ' pctTuile(2, 3).Image = pctTuile(2, 2).Image
        For oui = 0 To 9
            If sender.Name = ("pctTuile" & oui.ToString & "0") Then
                For p = 0 To 29
                    For o = 0 To 29
                        If pctTuile(o, p).Name = sender.Name Then
                            pctTuile(o, p + 1).Image = image
                            sender.Image = Nothing
                            tableTuiles.Size = tableTuiles.Size + New System.Drawing.Size(0, 51)
                            fermela = True
                        End If
                    Next
                Next
            ElseIf sender.Name = ("pctTuile" & "0" & oui.ToString) Then
                For p = 0 To 29
                    For o = 0 To 29
                        If pctTuile(o, p).Name = sender.Name Then
                            pctTuile(o + 1, p).Image = image
                            sender.Image = Nothing
                            tableTuiles.Size = tableTuiles.Size + New System.Drawing.Size(50, 0)
                        End If
                    Next
                Next
            End If
        Next

    End Sub

    Private Sub pctTuile_MouseMove(sender As Object, e As MouseEventArgs)
        If e.Button = MouseButtons.Left Then
            sender.DoDragDrop(sender.Image, DragDropEffects.Move)
            sender.Image = Nothing
        End If
    End Sub

    Private Sub pctBoxPioche0_DragDrop(sender As Object, e As DragEventArgs) Handles pctBoxPioche5.DragDrop, pctBoxPioche4.DragDrop, pctBoxPioche3.DragDrop, pctBoxPioche2.DragDrop, pctBoxPioche1.DragDrop, pctBoxPioche0.DragDrop
        Dim image As Bitmap = DirectCast(e.Data.GetData(DataFormats.Bitmap, True), Bitmap)
        sender.Image = image
    End Sub

    Private Sub pctBoxPioche0_DragEnter(sender As Object, e As DragEventArgs) Handles pctBoxPioche5.DragEnter, pctBoxPioche4.DragEnter, pctBoxPioche3.DragEnter, pctBoxPioche2.DragEnter, pctBoxPioche1.DragEnter, pctBoxPioche0.DragEnter
        If e.Data.GetDataPresent(DataFormats.Bitmap) Then
            e.Effect = DragDropEffects.Move
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

End Class
